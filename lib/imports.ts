import { GoogleSpreadsheet } from 'google-spreadsheet';
import { trim } from 'lodash';
import { Game, Logger, Player, Slot, Wish, WISH_LEVELS } from './models';

const A = 0;
const B = 1;
const C = 2;
const D = 3;
const E = 4;
const F = 5;
const G = 6;
const H = 7;
const I = 8;
const J = 9;
const K = 10;
const L = 11;
const M = 12;
const N = 13;
export const STATIC_COLUMNS = [A, B, C, D, E, F, G, H, I, J, K, L, M, N];
const GAME_NAME1 = A;
const MJ = B;
const GAME_NAME2 = C;
export const GAME_PRIORITY = D;
const MIN_PLAYERS = K;
const MAX_PLAYERS = L;
const GAME_SESSIONS = M;

export async function importSpreadsheet(logger: Logger): Promise<Imports> {
  const gsheetId = process.env.GOOGLE_SHEET_ID;
  if (!gsheetId) {
    throw new Error('Missing env var (GOOGLE_SHEET_ID)');
  }

  const doc = new GoogleSpreadsheet(gsheetId);

  const apiKey = process.env.GOOGLE_API_KEY;
  if (!apiKey) {
    throw new Error('Missing env var (GOOGLE_API_KEY)');
  }

  try {
    doc.useApiKey(apiKey);
  } catch (error) {
    logger.error(`An error happened while authenticating: ${error.message}`);
    throw new Error('Error while authenticating');
  }

  try {
    await doc.loadInfo();
  } catch (error) {
    logger.error(`An error happened while reading GSheet: ${error.message}`);
    throw new Error('Error while reading GSheet');
  }

  const sheet = doc.sheetsByIndex[0];
  const rows = await sheet.getRows();

  const players: Player[] = preparePlayers(sheet.headerValues);
  const games: Game[] = [];
  const wishes: RawWish[] = [];
  for (let i = 0; i < rows.length; ++i) {
    const number = rows[i]._rowNumber;
    const row = rows[i]._rawData;
    if (number <= 2 || !row || !row[0]) {
      continue;
    }

    prepareRow(number, row, games, players, wishes);
  }

  importRawWishesInPlayersAndGames(wishes, players, games);

  return {
    players,
    games,
  };
}

export function preparePlayers(data: string[]): Player[] {
  return data.reduce((acc: Player[], data) => {
    const match = data.match(/^(\d{2})- ([^\(]+)(?:\(([^\)]+)\))?/);
    if (!match) {
      return acc;
    }

    const player: Player = {
      id: parseInt(match[1]),
      name: trim(match[2]),
      pseudo: match[3] ? trim(match[3]) : match[2].split(' ')[0],
      wishes: [],
    };
    acc.push(player);
    return acc;
  }, []);
}

export function prepareRow(number: number, row: string[], games: Game[], players: Player[], wishes: RawWish[]): void {
  if (row[GAME_PRIORITY].match('HORS CRENEAU')) {
    return;
  }

  const game: Game = {
    id: number,
    name: `${row[GAME_NAME1]} - ${row[GAME_NAME2]}`,
    players: {
      minNumber: parseInt(row[MIN_PLAYERS]),
      maxNumber: parseInt(row[MAX_PLAYERS]),
    },
    gmId: players.find((player) => player.pseudo === trim(row[MJ]))?.id ?? null,
    slotsToAvoid: [],
    sessions: getSession(row[GAME_SESSIONS]),
    wishLevel: isNeedRawWish(row[GAME_PRIORITY]) ? WISH_LEVELS.NEED : WISH_LEVELS.WISH,
    wishes: [],
  };
  games.push(game);

  for (let i = STATIC_COLUMNS.length; i < row.length; ++i) {
    if (!row[i]) {
      continue;
    }

    const rawWish: RawWish = { playerId: i - STATIC_COLUMNS.length + 1, gameId: number, level: isNeedRawWish(row[i]) ? WISH_LEVELS.NEED : WISH_LEVELS.WISH };
    const xorMatch = row[i].match(/^ *(?:VEUT|PEUT)\(([^\)]+)\) */);
    if (xorMatch) {
      rawWish.xorName = xorMatch[1];
    }
    wishes.push(rawWish);
  };
}

function isNeedRawWish(data: string): boolean {
  return ['***', 'X', 'VEUT'].includes(trim(data)) || Boolean(data.match(/^ *VEUT\(.+\) *$/));
}

function getSession(sessionString: string): ({ minNumber: number, maxNumber: number }) {
  const sessionMatch = sessionString.match(/^(\d)(.*)$/);
  const minNumber = sessionMatch?.[1] ? parseInt(sessionMatch[1]) : 1;
  return {
    minNumber: minNumber,
    maxNumber: sessionMatch?.[2] ? minNumber + 1 : minNumber,
  };
}

export function importRawWishesInPlayersAndGames(wishes: RawWish[], players: Player[], games: Game[]): void {
  wishes.forEach((wish) => {
    const wishPlayer = players.find(player => player.id === wish.playerId);
    if (!wishPlayer) {
      throw new Error(`Unknow player ${wish.playerId} found in wishes`);
    }

    let linkedWishes = wishes.filter((wishToFilter) => wish.xorName && wishToFilter.playerId === wish.playerId &&
      wishToFilter.xorName === wish.xorName && wishToFilter !== wish);
    if (linkedWishes) {
      let sameLevel = true;
      for (let linkedWish of linkedWishes) {
        if (linkedWish.level !== wish.level) {
          sameLevel = false;
          break;
        }
      }

      if (!sameLevel) {
        linkedWishes = [];
      }
    }

    wishPlayer.wishes = wishPlayer.wishes ?? [];
    const playerWish = new Wish(wish.gameId, wish.level);
    if (linkedWishes.length) {
      playerWish.xor = linkedWishes.map((linkedWish) => linkedWish.gameId);
    }
    wishPlayer.wishes.push(playerWish);

    const wishGame = games.find(game => game.id === wish.gameId);
    if (!wishGame) {
      throw new Error(`Unknow game ${wish.gameId} found in wishes`);
    }

    wishGame.wishes = wishGame.wishes ?? [];
    wishGame.wishes.push(new Wish(wish.playerId, wish.level));
  });
}

export async function importJson(logger: Logger): Promise<Imports> {
  logger.info('Loading players');
  const partialPlayers = require('./players.json') as Omit<Player, 'wishes'>[];
  logger.debug(`Found ${partialPlayers.length} players`);

  logger.info('Loading games');
  const partialGames = require('./games.json') as Omit<Game, 'wishes'>[];
  logger.debug(`Found ${partialGames.length} games`);

  logger.info('Loading wishes');
  const wishes = require('./wishes.json') as RawWish[];
  logger.debug(`Found ${wishes.length} wishes`);

  const players = partialPlayers.map((partialPlayer): Player => ({
    ...partialPlayer,
    wishes: [],
  }));
  const games = partialGames.map((partialGame): Game => ({
    ...partialGame,
    wishes: [],
  }));

  importRawWishesInPlayersAndGames(wishes, players, games);

  return {
    players,
    games,
  };
}

export function importSlotsFromJson(logger: Logger): Slot[] {
  logger.info('Loading slots');
  const slots = require('../slots.json');
  logger.debug(`Found ${slots.length} slots`);
  return slots;
}

interface Imports {
  games: Game[],
  players: Player[],
}

export interface RawWish {
  gameId: number,
  playerId: number,
  level: WISH_LEVELS,
  xorName?: string,
}
