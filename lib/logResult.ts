import { BestGameReport, GameRequest, Logger, Player, WISH_LEVELS } from './models';
import * as _ from 'lodash';

export default function (request: GameRequest, report: BestGameReport | null, logger: Logger) {
  if (!report) {
    logger.warn('No result found!');
    return;
  }

  logger.info(`Best score: ${report.bestScore} (max: ${Math.max(...report.scores)}, avg: ${_.sum(report.scores) / report.scores.length})`);
  const schedule = report?.best;
  logger.info('********************');

  const resultsBySlots = _.groupBy(schedule.results, 'slot.id');
  Object.keys(resultsBySlots).forEach((slotId) => {
    logger.info(`#########\nslot ${slotId}\n#########`);

    resultsBySlots[slotId].forEach((result) => {
      logger.info(`For slot ${result.slot.id}: Game ${result.game.name} [${getWishLevel(result.game.wishLevel)}]` + (result.gm ? ` (GM: ${result.gm.name})` : ''));
      logger.info('Players:');
      result.players.forEach((player) => {
        logger.info(`  - ${player.name} [${getWishLevel(player.wishes.find((wish) => wish.id === result.game.id)?.level)}]`);
      });
      logger.info('');
    });
  });
  logger.info('********************');

  if (schedule.leftGames.length) {
    logger.warn('Some games cannot be placed:');
    [...schedule.leftGames].sort((a) => a.wishLevel === WISH_LEVELS.NEED ? -1 : 1).forEach((game) => {
      logger.info(`  - Game ${game.name} [${getWishLevel(game.wishLevel)}]` + (game.gmId ? ` (GM: ${
        request.players.find(playerToFind => playerToFind.id === game.gmId)?.name})` : ''));
    });
    logger.info('\n********************');
  }

  if (schedule.leftPlayersBySlot && Object.keys(schedule.leftPlayersBySlot).length) {
    logger.warn('Some players have empty slots:');
    request.slots.sort((a, b) => a.order > b.order ? 1 : -1).forEach((slot) => {
      if (!schedule.leftPlayersBySlot[slot.id].length) {
        return;
      }

      logger.info(`#########\nFor slot ${slot.id}:`);
      logger.info('Players:');
      schedule.leftPlayersBySlot[slot.id].forEach((player) => {
        logger.info(`  - ${player.name}`);
      });
      logger.info('');
    });
  }
}

export function logPlayerResult(player: Player, request: GameRequest, report: BestGameReport | null, logger: Logger) {
  if (!report) {
    logger.warn('No result found!');
    return;
  }

  const schedule = report?.best;
  logger.info(`Player ${player.name} schedule`);
  request.slots.sort((a, b) => a.order > b.order ? 1 : -1).forEach((slot) => {
    const result = schedule.results.find((result) => slot === result.slot &&
      (result.gm === player || result.players.includes(player)));
    if (result) {
      logger.info(`For slot ${slot.id}: Game ${
        request.games.find(gameToFind => gameToFind === result.game)?.name}` + (result.gm ? ` (GM: ${
          request.players.find(playerToFind => playerToFind === result.gm)?.name})` : ''));
      logger.info('Players:');
      result.players.forEach((player) => {
        logger.info(`  - ${request.players.find(playerToFind => playerToFind === player)?.name}`);
      });
      logger.info('');
      return;
    }

    logger.info(`For slot ${slot.id}: No game scheduled yet`);
    logger.info('Other potential available players:');
    const emptySlotPlayers = schedule.leftPlayersBySlot[slot.id]
      .filter((emptySlotPlayer) => emptySlotPlayer !== player);
    emptySlotPlayers.forEach((emptySlotPlayer) => {
      logger.info(`  - ${emptySlotPlayer.name}`);
    });
    logger.info('');
  });

  if (schedule.leftGames.length) {
    logger.warn('Some games are still potentially available:');
    schedule.leftGames.forEach((game) => {
      logger.info(`  - Game ${game.name}` + (game.gmId ? ` (GM: ${
          request.players.find(playerToFind => playerToFind.id === game.gmId)?.name})` : ''));
    });
  }
}

function getWishLevel(wishLevel?: WISH_LEVELS): string {
  if (!wishLevel) {
    return '';
  }

  if (wishLevel === WISH_LEVELS.NEED) {
    return 'VEUT';
  }

  return 'PEUT';
}
