import { workerData, parentPort } from 'worker_threads';
import { randoms, loggers } from './testFixtures';
import { getSchedule } from './getSchedule';

async function main() {
  parentPort?.postMessage(await getSchedule(workerData, randoms[workerData.randomName] ?? loggers.firstPlayer, loggers[workerData.loggerName] ?? loggers.console));
}

main();
