import {
  BestGameReport,
  GameResult,
  GameSchedule,
  Player,
  Wish,
  WISH_LEVELS,
} from './models';

const LEFT_PLAYERS_WEIGHT = 100000;
const LEFT_NEED_GAMES_WEIGHT = 100000;
const LEFT_WISH_GAMES_WEIGHT = 1000;
const LEFT_SLOTS_WEIGHT = 100;
const NEED_WISHES = 10000;
const WISH_WISHES = 1;

export default function getBestSchedule(schedules: GameSchedule[], players: Player[]): BestGameReport | null {
  if (!schedules.length) {
    return null;
  }

  const scores = [];
  for (let i = 0; i < schedules.length; ++i) {
    scores.push({
      score: getScore(schedules[i], players),
      schedule: schedules[i],
    });
  }

  const best = scores.sort((a, b) => a.score >= b.score ? 1 : -1)[0];

  return {
    best: best.schedule,
    bestScore: best.score,
    scores: scores.map((score) => score.score),
  };
}

function getScore(schedule: GameSchedule, players: Player[]): number {
  const score = LEFT_PLAYERS_WEIGHT * schedule.leftPlayers.length +
    LEFT_NEED_GAMES_WEIGHT * schedule.leftGames.filter((game) => game.wishLevel === WISH_LEVELS.NEED).length +
    LEFT_WISH_GAMES_WEIGHT * schedule.leftGames.filter((game) => game.wishLevel === WISH_LEVELS.WISH).length +
    LEFT_SLOTS_WEIGHT * schedule.leftSlots.length +
    NEED_WISHES * getUnsatisfiedWishesNumber(schedule.results, players, WISH_LEVELS.NEED) +
    WISH_WISHES * getUnsatisfiedWishesNumber(schedule.results, players, WISH_LEVELS.WISH)
  ;

  return score;
}

export function getUnsatisfiedWishesNumber(results: GameResult[], players: Player[], filter?: WISH_LEVELS): number {
  const playersWishes = players.reduce((acc: Record<string, Wish[]>, player) => {
    acc[player.id] = filter ? player.wishes.filter(wish => wish.level === filter) : [...player.wishes];
    return acc;
  }, {});

  results.forEach((result) => {
    result.players.forEach((player) => {
      const wishIndex = playersWishes[player.id].findIndex((wish) => wish.id === result.game.id);
      if (wishIndex === -1) { // filtered wish
        return;
      }
      playersWishes[player.id].splice(wishIndex, 1);
    });
  });

  return Object.values(playersWishes).reduce((acc, playerWishes) => {
    return acc + playerWishes.length;
  }, 0);
}
