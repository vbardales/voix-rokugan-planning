import { getScoreByPlayerState } from './playersStateRegistry';
import { CollectionRandomizer, DAY_POSITION, Game, Logger, Player, Randomizer, Score, ScoreRandomizer, Slot, WEEK_POSITION, WISH_LEVELS } from './models';
import * as _ from 'lodash';

/* Logger */
export const logger: Logger = {
  name: 'silent',
  log: () => null,
  info: () => null,
  warn: () => null,
  debug: () => null,
  error: () => null,
};
export const loggers: Record<string, Logger> = {
  silent: logger,
  console: console,
};

/* Players */
export const nelimPlayer: Player = {
  id: 1,
  name: 'Nelim',
  pseudo: 'Nelim',
  wishes: [
    { id: 1, level: WISH_LEVELS.NEED, xor: [] },
    { id: 2, level: WISH_LEVELS.NEED, xor: [] },
    { id: 3, level: WISH_LEVELS.NEED, xor: [] },
  ],
};

export const kevPlayer: Player = {
  id: 2,
  name: 'Irazetsu',
  pseudo: 'Irazetsu',
  wishes: [
    { id: 1, level: WISH_LEVELS.NEED, xor: [] },
    { id: 2, level: WISH_LEVELS.NEED, xor: [] },
    { id: 3, level: WISH_LEVELS.NEED, xor: [] },
  ],
};

export const iniginPlayer: Player = {
  id: 3,
  name: 'Kakita Inigin',
  pseudo: 'Inigin',
  wishes: [
    { id: 1, level: WISH_LEVELS.NEED, xor: [] },
    { id: 2, level: WISH_LEVELS.NEED, xor: [] },
    { id: 3, level: WISH_LEVELS.NEED, xor: [] },
  ],
};

/* Games */
export const defaultGame: Game = {
  id: 1,
  name: 'Game',
  players: {
    minNumber: 1,
    maxNumber: 1,
  },
  gmId: null,
  slotsToAvoid: [],
  wishLevel: WISH_LEVELS.NEED,
  wishes: [
    { id: 1, level: WISH_LEVELS.NEED, xor: [] },
    { id: 2, level: WISH_LEVELS.NEED, xor: [] },
    { id: 3, level: WISH_LEVELS.NEED, xor: [] },
  ],
  sessions: {
    minNumber: 1,
    maxNumber: 1,
  },
};

export const game2: Game = {
  id: 2,
  name: 'Game 2',
  players: {
    minNumber: 1,
    maxNumber: 2,
  },
  gmId: null,
  slotsToAvoid: [],
  wishLevel: WISH_LEVELS.NEED,
  wishes: [
    { id: 1, level: WISH_LEVELS.NEED, xor: [] },
    { id: 2, level: WISH_LEVELS.NEED, xor: [] },
    { id: 3, level: WISH_LEVELS.NEED, xor: [] },
  ],
  sessions: {
    minNumber: 1,
    maxNumber: 1,
  },
};

export const multiSlotGame: Game = {
  id: 3,
  name: '2-slots game',
  players: {
    minNumber: 1,
    maxNumber: 2,
  },
  gmId: null,
  slotsToAvoid: [],
  wishLevel: WISH_LEVELS.NEED,
  wishes: [
    { id: 1, level: WISH_LEVELS.NEED, xor: [] },
    { id: 2, level: WISH_LEVELS.NEED, xor: [] },
    { id: 3, level: WISH_LEVELS.NEED, xor: [] },
  ],
  sessions: {
    minNumber: 2,
    maxNumber: 2,
  },
};

/* Slots */
export const saturdayNightSlot: Slot = {
  id: 'samediSoir',
  order: 0,
  weekPosition: WEEK_POSITION.SATURDAY,
  dayPosition: DAY_POSITION.NIGHT,
  prev: null,
  next: 'dimancheMidi',
};

export const sundayEveningSlot: Slot = {
  id: 'dimancheMidi',
  order: 1,
  weekPosition: WEEK_POSITION.SUNDAY,
  dayPosition: DAY_POSITION.EVENING,
  prev: 'samediSoir',
  next: 'dimancheSoir',
};

export const sundayNightSlot: Slot = {
  id: 'dimancheSoir',
  order: 2,
  weekPosition: WEEK_POSITION.SUNDAY,
  dayPosition: DAY_POSITION.NIGHT,
  prev: 'dimancheMidi',
  next: 'lundiMidi',
};

export const mondayEveningSlot: Slot = {
  id: 'lundiMidi',
  order: 3,
  weekPosition: WEEK_POSITION.MONDAY,
  dayPosition: DAY_POSITION.EVENING,
  prev: 'dimancheSoir',
  next: 'lundiSoir',
};

export const mondayNightSlot: Slot = {
  id: 'lundiSoir',
  order: 4,
  weekPosition: WEEK_POSITION.MONDAY,
  dayPosition: DAY_POSITION.NIGHT,
  prev: 'lundiMidi',
  next: null,
};

/* Random */
export const noRandomizeRandom: CollectionRandomizer = {
  name: 'noRandomize',
  getCollection: (collection) => [...collection],
};

export const shufflingRandom: CollectionRandomizer = {
  name: 'shuffling',
  getCollection: (collection) => _.shuffle(collection),
};

export const lastPlayerRandom: ScoreRandomizer = {
  name: 'lastPlayer',
  getScore: (players: Player[]): Score => ({
    player: players[players.length - 1],
    diff: 0,
    score: 0,
    previousScore: 0,
  }),
};

export const byPlayerStateRandom: Randomizer = {
  name: 'byPlayerState',
  getScore: getScoreByPlayerState,
};

export const firstPlayerRandom: ScoreRandomizer = {
  name: 'firstPlayer',
  getScore: (players: Player[]): Score => ({
    player: players[0],
    diff: 0,
    score: 0,
    previousScore: 0,
  }),
};

export const randoms: Record<string, ScoreRandomizer> = {
  firstPlayer: firstPlayerRandom,
  lastPlayer: lastPlayerRandom,
  byPlayerState: byPlayerStateRandom,
};
