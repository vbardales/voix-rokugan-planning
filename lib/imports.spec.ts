import { preparePlayers, prepareRow, STATIC_COLUMNS, RawWish, GAME_PRIORITY, importRawWishesInPlayersAndGames } from './imports';
import { Game, WISH_LEVELS } from './models';

const defaultPlayerRow = [
  'JdR',
  'MJ',
  'Scénar',
  'Priorité du MJ',
  'Joueurs intéressés',
  'OPEN / FULL',
  'VEUT',
  'PEUT',
  'Public',
  'Prétirés ou pas',
  'Nb de PJ min',
  'Nb de PJ max',
  'Nb de sessions nécessaires',
  'Style ou thématique',
  '01- Kévin C. (Kev)',
  '06- Yannic B. ',
  '12- Sophie',
  '22- Loïc C. (Loic)',
];

const defaultRow = [
  'D&D5',
  'Kev',
  'Lost Mine Of Phandelver',
  'PEUT',
  '4',
  'OPEN',
  '0',
  '1',
  'Tout public',
  'Oui',
  '3',
  '5',
  '3+',
  'Old School revival',
  '',
  '',
  '',
  '*',
  '',
  '',
  '',
  '',
  '',
  '',
  '',
  '',
  '',
  '',
  '',
  '',
  '',
  '',
  '',
  '',
  '',
  '',
  '',
  '',
  '',
];

describe('imports', () => {
  describe('preparePlayer', () => {
    it('should generate given players and ignoring all other fields', () => {
      expect(preparePlayers(defaultPlayerRow)).toEqual([
        { id: 1, name: 'Kévin C.', pseudo: 'Kev', wishes: [] },
        { id: 6, name: 'Yannic B.', pseudo: 'Yannic', wishes: [] },
        { id: 12, name: 'Sophie', pseudo: 'Sophie', wishes: [] },
        { id: 22, name: 'Loïc C.', pseudo: 'Loic', wishes: [] },
      ]);
    });
  });

  describe('prepareRow', () => {
    it('should create the given game', () => {
      const players = [
        { id: 1, name: 'Kévin C.', pseudo: 'Kev', wishes: [] },
        { id: 6, name: 'Yannic B.', pseudo: 'Yannic', wishes: [] },
        { id: 12, name: 'Sophie', pseudo: 'Sophie', wishes: [] },
        { id: 22, name: 'Loïc C.', pseudo: 'Loic', wishes: [] },
      ];
      const games: Game[] = [];
      const wishes: RawWish[] = [];
      const row = [...defaultRow];
      row[STATIC_COLUMNS.length + 3] = '*';
      row[STATIC_COLUMNS.length + 4] = '*';
      row[STATIC_COLUMNS.length + 23] = '*';
      row[STATIC_COLUMNS.length + 24] = '*';

      expect(() => prepareRow(3, row, games, players, wishes)).not.toThrow();

      expect(games).toHaveLength(1);
      expect(games[0]).toEqual({
        id: 3,
        name: 'D&D5 - Lost Mine Of Phandelver',
        players: {
          minNumber: 3,
          maxNumber: 5,
        },
        gmId: 1,
        slotsToAvoid: [],
        sessions: {
          minNumber: 3,
          maxNumber: 4,
        },
        wishLevel: WISH_LEVELS.WISH,
        wishes: [],
      });

      expect(wishes).toHaveLength(4);
      expect(wishes).toEqual([
        { playerId: 4, gameId: 3, level: WISH_LEVELS.WISH },
        { playerId: 5, gameId: 3, level: WISH_LEVELS.WISH },
        { playerId: 24, gameId: 3, level: WISH_LEVELS.WISH },
        { playerId: 25, gameId: 3, level: WISH_LEVELS.WISH },
      ]);
    });

    it('should not create the given game if HORS CRENEAU', () => {
      const players = [
        { id: 1, name: 'Kévin C.', pseudo: 'Kev', wishes: [] },
      ];
      const games: Game[] = [];
      const wishes: RawWish[] = [];
      const row = [...defaultRow];
      row[GAME_PRIORITY] = 'HORS CRENEAU';

      expect(() => prepareRow(3, row, games, players, wishes)).not.toThrow();

      expect(games).toHaveLength(0);
    });

    describe('wishes', () => {
      describe('NEED', () => {
        it('should set wish as NEED if ***', () => {
          const players = [
            { id: 1, name: 'Kévin C.', pseudo: 'Kev', wishes: [] },
          ];
          const games: Game[] = [];
          const wishes: RawWish[] = [];

          const row = [...defaultRow];
          row[GAME_PRIORITY] = '***';
          row[STATIC_COLUMNS.length + 3] = '***';

          prepareRow(3, row, games, players, wishes);

          expect(games[0].wishLevel).toEqual(WISH_LEVELS.NEED);
          expect(wishes).toEqual([
            { playerId: 4, gameId: 3, level: WISH_LEVELS.NEED },
          ]);
        });

        it('should set wish as NEED if X', () => {
          const players = [
            { id: 1, name: 'Kévin C.', pseudo: 'Kev', wishes: [] },
          ];
          const games: Game[] = [];
          const wishes: RawWish[] = [];

          const row = [...defaultRow];
          row[GAME_PRIORITY] = 'X'
          row[STATIC_COLUMNS.length + 3] = 'X';

          prepareRow(3, row, games, players, wishes);

          expect(games[0].wishLevel).toEqual(WISH_LEVELS.NEED);
          expect(wishes).toEqual([
            { playerId: 4, gameId: 3, level: WISH_LEVELS.NEED },
          ]);
        });

        it('should set wish as NEED if VEUT', () => {
          const players = [
            { id: 1, name: 'Kévin C.', pseudo: 'Kev', wishes: [] },
          ];
          const games: Game[] = [];
          const wishes: RawWish[] = [];

          const row = [...defaultRow];
          row[GAME_PRIORITY] = 'VEUT';
          row[STATIC_COLUMNS.length + 3] = 'VEUT';

          prepareRow(3, row, games, players, wishes);

          expect(games[0].wishLevel).toEqual(WISH_LEVELS.NEED);
          expect(wishes).toEqual([
            { playerId: 4, gameId: 3, level: WISH_LEVELS.NEED },
          ]);
        });
      });

      it('should set wish as WISH if whatever but not an empty string', () => {
        const players = [
          { id: 1, name: 'Kévin C.', pseudo: 'Kev', wishes: [] },
        ];
        const games: Game[] = [];
        const wishes: RawWish[] = [];

        const row = [...defaultRow];
        row[GAME_PRIORITY] = '*';
        row[STATIC_COLUMNS.length + 3] = '*';

        prepareRow(3, row, games, players, wishes);

        expect(games[0].wishLevel).toEqual(WISH_LEVELS.WISH);
        expect(wishes).toEqual([
          { playerId: 4, gameId: 3, level: WISH_LEVELS.WISH },
        ]);
      });

      it('should not set wish if empty', () => {
        const players = [
          { id: 1, name: 'Kévin C.', pseudo: 'Kev', wishes: [] },
        ];
        const games: Game[] = [];
        const wishes: RawWish[] = [];

        const row = [...defaultRow];
        row[STATIC_COLUMNS.length + 3] = '';

        prepareRow(3, row, games, players, wishes);

        expect(wishes).toEqual([]);
      });

      it('should set game wishLevel even if empty and default to WISH', () => {
        const players = [
          { id: 1, name: 'Kévin C.', pseudo: 'Kev', wishes: [] },
        ];
        const games: Game[] = [];
        const wishes: RawWish[] = [];

        const row = [...defaultRow];
        row[GAME_PRIORITY] = '';
        row[STATIC_COLUMNS.length + 3] = '';

        prepareRow(3, row, games, players, wishes);

        expect(games[0].wishLevel).toEqual(WISH_LEVELS.WISH);
      });
    });

    describe('XOR', () => {
      it('should handle XOR games', () => {
        const players = [
          { id: 1, name: 'Kévin C.', pseudo: 'Kev', wishes: [] },
          { id: 6, name: 'Yannic B.', pseudo: 'Yannic', wishes: [] },
          { id: 12, name: 'Sophie', pseudo: 'Sophie', wishes: [] },
          { id: 22, name: 'Loïc C.', pseudo: 'Loic', wishes: [] },
        ];
        const games: Game[] = [];
        const wishes: RawWish[] = [];
        const row = [...defaultRow];
        row[STATIC_COLUMNS.length + 3] = 'VEUT(XOR1)';
        row[STATIC_COLUMNS.length + 4] = 'VEUT(XOR1)';
        row[STATIC_COLUMNS.length + 23] = 'PEUT(XOR2)';
        row[STATIC_COLUMNS.length + 24] = 'PEUT(XOR2)';
        row[STATIC_COLUMNS.length + 25] = 'PEUT';

        prepareRow(3, row, games, players, wishes);
        expect(wishes).toEqual([
          { playerId: 4, gameId: 3, level: WISH_LEVELS.NEED, xorName: 'XOR1' },
          { playerId: 5, gameId: 3, level: WISH_LEVELS.NEED, xorName: 'XOR1' },
          { playerId: 24, gameId: 3, level: WISH_LEVELS.WISH, xorName: 'XOR2' },
          { playerId: 25, gameId: 3, level: WISH_LEVELS.WISH, xorName: 'XOR2' },
          { playerId: 26, gameId: 3, level: WISH_LEVELS.WISH },
        ]);
      });
    });
  });

  describe('importRawWishesInPlayersAndGames', () => {
    it('should link wishes in games and players', () => {
      const wishes = [
        { playerId: 5, gameId: 3, level: WISH_LEVELS.WISH },
        { playerId: 24, gameId: 3, level: WISH_LEVELS.NEED },
        { playerId: 25, gameId: 3, level: WISH_LEVELS.WISH },
        { playerId: 25, gameId: 4, level: WISH_LEVELS.WISH },
      ];
      const players = [
        { id: 1, name: 'Kévin C.', pseudo: 'Kev', wishes: [] },
        { id: 5, name: 'Yannic B.', pseudo: 'Yannic', wishes: [] },
        { id: 24, name: 'Sophie', pseudo: 'Sophie', wishes: [] },
        { id: 25, name: 'Loïc C.', pseudo: 'Loic', wishes: [] },
      ];
      const games = [{
        id: 1,
        name: 'Noplayersgame',
        players: {
          minNumber: 3,
          maxNumber: 5,
        },
        gmId: 1,
        slotsToAvoid: [],
        sessions: {
          minNumber: 3,
          maxNumber: 4,
        },
        wishLevel: WISH_LEVELS.WISH,
        wishes: [],
      },
      {
        id: 3,
        name: 'D&D5 - Lost Mine Of Phandelver',
        players: {
          minNumber: 3,
          maxNumber: 5,
        },
        gmId: 1,
        slotsToAvoid: [],
        sessions: {
          minNumber: 3,
          maxNumber: 4,
        },
        wishLevel: WISH_LEVELS.WISH,
        wishes: [],
      }, {
          id: 4,
          name: 'AnotherGame',
          players: {
            minNumber: 3,
            maxNumber: 5,
          },
          gmId: 1,
          slotsToAvoid: [],
          sessions: {
            minNumber: 3,
            maxNumber: 4,
          },
          wishLevel: WISH_LEVELS.WISH,
          wishes: [],
        }];

      importRawWishesInPlayersAndGames(wishes, players, games);

      expect(players[0].wishes).toEqual([]);
      expect(players[1].wishes).toEqual([{ id: 3, level: WISH_LEVELS.WISH, xor: [] }]);
      expect(players[2].wishes).toEqual([{ id: 3, level: WISH_LEVELS.NEED, xor: [] }]);
      expect(players[3].wishes).toEqual([{ id: 3, level: WISH_LEVELS.WISH, xor: [] }, { id: 4, level: WISH_LEVELS.WISH, xor: [] }]);

      expect(games[0].wishes).toEqual([]);
      expect(games[1].wishes).toEqual([{ id: 5, level: WISH_LEVELS.WISH, xor: [] }, { id: 24, level: WISH_LEVELS.NEED, xor: [] }, { id: 25, level: WISH_LEVELS.WISH, xor: [] }]);
      expect(games[2].wishes).toEqual([{ id: 25, level: WISH_LEVELS.WISH, xor: [] }]);
    });

    it('should link XOR games if any', () => {
      const wishes = [
        // XOR on different levels (XOR is ignored)
        { playerId: 1, gameId: 1, level: WISH_LEVELS.WISH, xorName: 'XOR1' },
        { playerId: 1, gameId: 3, level: WISH_LEVELS.NEED, xorName: 'XOR1' },
        // same name XOR (like other player)
        { playerId: 5, gameId: 3, level: WISH_LEVELS.WISH, xorName: 'XOR1' },
        { playerId: 5, gameId: 4, level: WISH_LEVELS.WISH, xorName: 'XOR1' },
        // triple XOR
        { playerId: 24, gameId: 1, level: WISH_LEVELS.WISH, xorName: 'XOR1' },
        { playerId: 24, gameId: 3, level: WISH_LEVELS.WISH, xorName: 'XOR1' },
        { playerId: 24, gameId: 4, level: WISH_LEVELS.WISH, xorName: 'XOR1' },
        // two XORs on different games
        { playerId: 25, gameId: 1, level: WISH_LEVELS.NEED, xorName: 'XOR2' },
        { playerId: 25, gameId: 2, level: WISH_LEVELS.NEED, xorName: 'XOR2' },
        { playerId: 25, gameId: 3, level: WISH_LEVELS.WISH, xorName: 'XOR1' },
        { playerId: 25, gameId: 4, level: WISH_LEVELS.WISH, xorName: 'XOR1' },
      ];
      const players = [
        { id: 1, name: 'Kévin C.', pseudo: 'Kev', wishes: [] },
        { id: 5, name: 'Yannic B.', pseudo: 'Yannic', wishes: [] },
        { id: 24, name: 'Sophie', pseudo: 'Sophie', wishes: [] },
        { id: 25, name: 'Loïc C.', pseudo: 'Loic', wishes: [] },
      ];
      const games = [{
        id: 1,
        name: 'randomGame1',
        players: {
          minNumber: 3,
          maxNumber: 5,
        },
        gmId: 1,
        slotsToAvoid: [],
        sessions: {
          minNumber: 3,
          maxNumber: 4,
        },
        wishLevel: WISH_LEVELS.WISH,
        wishes: [],
      },{
        id: 2,
        name: 'randomGame2',
        players: {
          minNumber: 3,
          maxNumber: 5,
        },
        gmId: 1,
        slotsToAvoid: [],
        sessions: {
          minNumber: 3,
          maxNumber: 4,
        },
        wishLevel: WISH_LEVELS.WISH,
        wishes: [],
      },
      {
        id: 3,
        name: 'D&D5 - Lost Mine Of Phandelver',
        players: {
          minNumber: 3,
          maxNumber: 5,
        },
        gmId: 1,
        slotsToAvoid: [],
        sessions: {
          minNumber: 3,
          maxNumber: 4,
        },
        wishLevel: WISH_LEVELS.WISH,
        wishes: [],
      }, {
        id: 4,
        name: 'AnotherGame',
        players: {
          minNumber: 3,
          maxNumber: 5,
        },
        gmId: 1,
        slotsToAvoid: [],
        sessions: {
          minNumber: 3,
          maxNumber: 4,
        },
        wishLevel: WISH_LEVELS.WISH,
        wishes: [],
      }];

      importRawWishesInPlayersAndGames(wishes, players, games);

      expect(players[0].wishes).toEqual([{ id: 1, level: WISH_LEVELS.WISH, xor: [] }, { id: 3, level: WISH_LEVELS.NEED, xor: [] }]);
      expect(players[1].wishes).toEqual([{ id: 3, level: WISH_LEVELS.WISH, xor: [4] }, { id: 4, level: WISH_LEVELS.WISH, xor: [3] }]);
      expect(players[2].wishes).toEqual([{ id: 1, level: WISH_LEVELS.WISH, xor: [3, 4] }, { id: 3, level: WISH_LEVELS.WISH, xor: [1, 4] }, { id: 4, level: WISH_LEVELS.WISH, xor: [1, 3] }]);
      expect(players[3].wishes).toEqual([
        { id: 1, level: WISH_LEVELS.NEED, xor: [2] },
        { id: 2, level: WISH_LEVELS.NEED, xor: [1] },
        { id: 3, level: WISH_LEVELS.WISH, xor: [4] },
        { id: 4, level: WISH_LEVELS.WISH, xor: [3] },
      ]);
    });
  });
});
