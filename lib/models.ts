export interface Game {
  id: number,
  name: string,
  players: {
    minNumber: number,
    maxNumber: number,
  },
  gmId: number | null,
  slotsToAvoid: string[],
  wishLevel: WISH_LEVELS,
  wishes: Wish[],
  sessions: {
    minNumber: number,
    maxNumber: number,
  },
}

export enum WEEK_POSITION {
  SATURDAY = -4,
  SUNDAY = -3,
  MONDAY = -2,
  TUESDAY = -1,
  WENESDAY = 1,
  THURSDAY = 2,
  FRIDAY = 3,
}

export enum DAY_POSITION {
  EVENING = -1,
  NIGHT = 1,
}

export interface Slot {
  id: string,
  order: number,
  weekPosition: WEEK_POSITION,
  dayPosition: DAY_POSITION,
  prev: string | null,
  next: string | null,
}

export interface Player {
  id: number,
  name: string,
  pseudo: string,
  wishes: Wish[],
}

export interface GameRequest {
  games: Game[],
  slots: Slot[],
  players: Player[],
}

export interface GameSchedule {
  results: GameResult[],
  leftGames: Game[],
  leftSlots: Slot[],
  leftPlayers: Player[],
  leftPlayersBySlot: Record<string, Player[]>,
}

export interface GameComparison {
  schedules: GameSchedule[],
  bestReport: BestGameReport | null,
}

export interface GameResult {
  slot: Slot,
  game: Game,
  gm: Player | null,
  players: Player[],
}

export interface Logger {
  name?: string,
  log: (...args: unknown[]) => void,
  info: (...args: unknown[]) => void,
  warn: (...args: unknown[]) => void,
  debug: (...args: unknown[]) => void,
  error: (...args: unknown[]) => void,
}

export interface ScoreRandomizer {
  name: string,
  getScore(players: Player[], playersStateRegistry: PlayerStateRegistry, slot: Slot, game: Game): Score,
}

export interface CollectionRandomizer {
  name: string,
  getCollection<T>(collection: T[]): T[],
}

export type Randomizer = ScoreRandomizer | CollectionRandomizer;

export class PlayerStateRegistry {
  private registry: Record<string, PlayerState>;

  constructor(players: Player[]) {
    this.registry = players.reduce((acc: Record<string, PlayerState>, player) => {
      acc[player.id] = {
        weekPosition: 0, // saturday = -3, tuesday = 0, friday = 3
        dayPosition: 0, // evening = -1, night = 1
        gmOrNot: 0, // gm = 1, pc = -1
        played: 0,
      };
      return acc;
    }, {});
  }

  get(player: Player): PlayerState {
    if (!this.registry[player.id]) {
      throw new Error(`Player ${player.id} is unknown`);
    }

    return this.registry[player.id];
  }
}

export class PlayerState {
  public weekPosition: number = 0; // saturday = -3, tuesday = 0, friday = 3
  public dayPosition: number = 0; // evening = -1, night = 1
  public gmOrNot: number = 0; // gm = 1, pc = -1
  public played: number = 0;
}

export interface Score {
  previousScore: number,
  diff: number,
  score: number,
  player: Player,
}

export class Possibility {
  public gameSlots: Slot[];
  public isOk: boolean | null = null;

  constructor(originalSlot: Slot, gameSlots: Slot[]) {
    this.gameSlots = [...gameSlots].sort((a) => a === originalSlot ? -1 : 1);
  }

  toString(): string {
    return this.gameSlots.map((slot) => slot.id).join('-');
  }
};

export enum WISH_LEVELS {
  NEED = 'NEED',
  WISH = 'WISH',
}

export class Wish {
  constructor(public id: number, public level: WISH_LEVELS, public xor: number[] = []) {}

  static getAllWishIds(wishes: Wish[]): number[] {
    return wishes.map(wish => wish.id);
  }
  static getNeedWishIds(wishes: Wish[]): number[] {
    return wishes.filter(wish => wish.level === WISH_LEVELS.NEED).map(wish => wish.id);
  }
  static getWishWishIds(wishes: Wish[]): number[] {
    return wishes.filter(wish => wish.level === WISH_LEVELS.WISH).map(wish => wish.id);
  }
  static isWished(wishingEntity: Player | Game, wishedEntity: Player | Game, level?: WISH_LEVELS): boolean {
    let wishes;
    switch (level) {
      case WISH_LEVELS.NEED: {
        wishes = this.getNeedWishIds(wishingEntity.wishes);
        break;
      }
      case WISH_LEVELS.WISH: {
        wishes = this.getWishWishIds(wishingEntity.wishes);
        break;
      }
      default: {
        wishes = this.getAllWishIds(wishingEntity.wishes);
      }
    }


    return Boolean(wishes.find(wishId => wishId === wishedEntity.id));
  }
}

export interface BestGameReport {
  best: GameSchedule,
  bestScore: number,
  scores: number[]
}
