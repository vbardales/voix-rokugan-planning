import {
  Game,
  Player,
  PlayerStateRegistry,
  Score,
  Slot,
  Wish,
  WISH_LEVELS,
} from './models';
import * as _ from 'lodash';

export function updatePlayersStateRegistry(playersStateRegistry: PlayerStateRegistry, player: Player, slot: Slot, game: Game): void {
  const state = playersStateRegistry.get(player);
  state.weekPosition += slot.weekPosition;
  state.dayPosition += slot.dayPosition;
  state.gmOrNot += (game.gmId === player.id ? 1 : -1);
  ++state.played;
}

const WEEK_POSITION_WEIGHT = 1;
const DAY_POSITION_WEIGHT = 1;
const GM_OR_NOT_WEIGHT = 10;
const PLAYED_WEIGHT = 10;

export function getScoreByPlayerState(players: Player[], playersStateRegistry: PlayerStateRegistry, slot: Slot, game: Game): Score {
  if (!players) {
    throw new Error('Players are required');
  }

  const needPlayersPool = players.filter(player => Wish.isWished(player, game, WISH_LEVELS.NEED));
  const scores = (needPlayersPool.length ? needPlayersPool : players).map((player) => {
    const state = playersStateRegistry.get(player);

    const previousScore = WEEK_POSITION_WEIGHT * state.weekPosition +
      DAY_POSITION_WEIGHT * state.dayPosition +
      GM_OR_NOT_WEIGHT * state.gmOrNot +
      PLAYED_WEIGHT * state.played;
    const diff = WEEK_POSITION_WEIGHT * slot.weekPosition +
      DAY_POSITION_WEIGHT * slot.dayPosition +
      GM_OR_NOT_WEIGHT * -1 ;

    return {
      previousScore,
      diff: diff + (diff < 0 ? -1 : 1),
      score: previousScore + diff,
      player,
    };
  });

  const maxDiff = Math.max(...scores.map(score => Math.abs(score.diff)));
  const scoresWithMaxDiff = scores.filter(score => Math.abs(score.diff) === maxDiff);
  return _.maxBy(scoresWithMaxDiff, score => Math.abs(score.previousScore))!;
}
