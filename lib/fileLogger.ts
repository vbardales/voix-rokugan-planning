import * as pino from 'pino';
import *  as fs from 'fs';
import * as path from 'path';

export default function getNewLogger(pathString: string) {
  const pinoLogger = pino({
    prettyPrint: true,
    // @ts-ignore
    prettifier: () => (inputData) => `${inputData.msg}\n`,
  }, pino.destination(fs.openSync(path.join(__dirname, '..', pathString), 'w')));

  return {
    name: 'pino',
    log: pinoLogger.info.bind(pinoLogger),
    info: pinoLogger.info.bind(pinoLogger),
    warn: pinoLogger.warn.bind(pinoLogger),
    debug: pinoLogger.debug.bind(pinoLogger),
    error: pinoLogger.error.bind(pinoLogger),
  };
}
