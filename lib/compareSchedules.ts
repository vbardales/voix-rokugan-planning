import getBestSchedule from './getBestSchedule';
import { CollectionRandomizer, GameComparison, GameRequest, GameSchedule, Logger, ScoreRandomizer, Wish, WISH_LEVELS } from './models';
import * as bluebird from 'bluebird';

export default async function compareSchedules(request: GameRequest, {
  logger,
  scoreRandom,
  collectionRandom,
}: {
  logger: Logger,
  scoreRandom: ScoreRandomizer,
  collectionRandom: CollectionRandomizer,
}, getSchedule: (request: GameRequest, scoreRandom: ScoreRandomizer, logger: Logger, name: string) => Promise<GameSchedule>, n: number): Promise<GameComparison> {
  logger.info(`Generating ${n} tests`);
  const tests = generateTests(request, n, collectionRandom);

  const schedules = await bluebird.map(tests, (request, i) => getSchedule(request, scoreRandom, logger, i.toString()), { concurrency: 4 });

  const bestReport = getBestSchedule(schedules, request.players);
  return {
    schedules,
    bestReport,
  };
}

export function generateTests(request: GameRequest, testNumber: number, random: CollectionRandomizer): GameRequest[] {
  const requests = [];
  for (let i = 0; i < testNumber; ++i) {
    const currentPlayers = random.getCollection(request.players).sort((a, b) => {
      if (Wish.getNeedWishIds(a.wishes).length > Wish.getNeedWishIds(b.wishes).length) {
        return -1;
      }

      return Wish.getWishWishIds(a.wishes).length > Wish.getWishWishIds(b.wishes).length ? -1 : 1;
    });

    const currentGames = random.getCollection(request.games).sort((a, b) => {
      if (a.wishLevel !== b.wishLevel) {
        return a.wishLevel === WISH_LEVELS.NEED ? -1 : 1;
      }

      if (a.sessions.maxNumber !== b.sessions.maxNumber) {
        return a.sessions.maxNumber > b.sessions.maxNumber ? -1 : 1;
      }

      if (Wish.getNeedWishIds(a.wishes).length !== Wish.getNeedWishIds(b.wishes).length) {
        return Wish.getNeedWishIds(a.wishes).length > Wish.getNeedWishIds(b.wishes).length ? -1 : 1;
      }

      if (Wish.getWishWishIds(a.wishes).length !== Wish.getWishWishIds(b.wishes).length) {
        return Wish.getWishWishIds(a.wishes).length > Wish.getWishWishIds(b.wishes).length ? -1 : 1;
      }

      return a.players.minNumber > b.players.minNumber ? -1 : 1;
    });

    const currentSlots = random.getCollection(request.slots);

    requests.push({
      ...request,
      players: currentPlayers,
      slots: currentSlots,
      games: currentGames,
    });
  }

  return requests;
}
