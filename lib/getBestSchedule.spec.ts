import getBestSchedule, { getUnsatisfiedWishesNumber } from './getBestSchedule';
import { WISH_LEVELS } from './models';
import { defaultGame, game2, nelimPlayer, saturdayNightSlot } from './testFixtures';

const schedule1 = {
  results: [],
  leftGames: [],
  leftSlots: [],
  leftPlayers: [],
  leftPlayersBySlot: {},
};

const players = [nelimPlayer];

describe('getBestSchedule', () => {
  it('should select no schedule if no given', () => {
    expect(getBestSchedule([], players)).toBeNull();
  });

  it('should select the schedule that have the less players not selected', () => {
    const onePlayerNotSelectedSchedule = {
      ...schedule1,
      leftPlayers: [nelimPlayer],
    };
    const noPlayerNotSelectedSchedule = schedule1;

    expect(getBestSchedule([
      onePlayerNotSelectedSchedule,
      noPlayerNotSelectedSchedule,
    ], players)).toEqual({
      best: noPlayerNotSelectedSchedule,
      bestScore: expect.any(Number),
      scores: [expect.any(Number), expect.any(Number)],
    });
  });

  it('should select the schedule that have the less games NEED not selected', () => {
    const needGame = {
      ...game2,
      wishLevel: WISH_LEVELS.NEED,
    };
    const wishGame1 = {
      ...game2,
      wishLevel: WISH_LEVELS.WISH,
    };
    const wishGame2 = {
      ...game2,
      wishLevel: WISH_LEVELS.WISH,
    };
    const leftLessNeedGamesSchedule = {
      ...schedule1,
      leftGames: [wishGame1, wishGame2],
    }
    expect(getBestSchedule([
      {
        ...schedule1,
        leftGames: [needGame],
      },
      leftLessNeedGamesSchedule,
    ], players)).toEqual({
      best: leftLessNeedGamesSchedule,
      bestScore: expect.any(Number),
      scores: [expect.any(Number), expect.any(Number)],
    });
  });

  it('should select the schedule that have the less games not selected (level-free) if same left players and NEED games', () => {
    expect(getBestSchedule([
      {
        ...schedule1,
        leftGames: [game2],
      },
      schedule1,
    ], players)).toEqual({
      best: schedule1,
      bestScore: expect.any(Number),
      scores: [expect.any(Number), expect.any(Number)],
    });
  });

  it('should select the schedule that have the less left slots not selected if same left players, games', () => {
    expect(getBestSchedule([
      {
        ...schedule1,
        leftSlots: [saturdayNightSlot],
      },
      schedule1,
    ], players)).toEqual({
      best: schedule1,
      bestScore: expect.any(Number),
      scores: [expect.any(Number), expect.any(Number)],
    });
  });

  it('should select the schedule that have the less NEED wishes in players wishes not selected if same left players, games, slots', () => {
    const shadowPlayer = {
      ...nelimPlayer,
      wishes: [{ id: 2, level: WISH_LEVELS.WISH, xor: [] }, { id: 1, level: WISH_LEVELS.NEED, xor: [] }],
    };
    const bestSchedule = {
      ...schedule1,
      results: [
        {
          game: defaultGame,
          slot: saturdayNightSlot,
          gm: null,
          players: [shadowPlayer],
        },
      ],
    };
    expect(getBestSchedule([
      {
        ...schedule1,
        results: [
          {
            game: game2,
            slot: saturdayNightSlot,
            gm: null,
            players: [shadowPlayer],
          },
        ],
      },
      bestSchedule,
    ], [shadowPlayer])).toEqual({
      best: bestSchedule,
      bestScore: expect.any(Number),
      scores: [expect.any(Number), expect.any(Number)],
    });
  });

  it('should select the schedule that have the less WISH wishes in players wishes not selected if same left players, games, slots', () => {
    const shadowPlayer = {
      ...nelimPlayer,
      wishes: [{ id: 1, level: WISH_LEVELS.WISH, xor: [] }],
    };
    const bestSchedule = {
      ...schedule1,
      results: [
        {
          game: defaultGame,
          slot: saturdayNightSlot,
          gm: null,
          players: [shadowPlayer],
        },
      ],
    };
    expect(getBestSchedule([
      {
        ...schedule1,
        results: [
          {
            game: game2,
            slot: saturdayNightSlot,
            gm: null,
            players: [shadowPlayer],
          },
        ],
      },
      bestSchedule,
    ], [shadowPlayer])).toEqual({
      best: bestSchedule,
      bestScore: expect.any(Number),
      scores: [expect.any(Number), expect.any(Number)],
    });
  });
});

describe('getUnsatisfiedWishesNumber', () => {
  it('should be 0 if all wishes are exauced', () => {
    const shadowPlayer = {
      ...nelimPlayer,
      wishes: [{ id: 1, level: WISH_LEVELS.NEED, xor: [] }],
    };

    expect(getUnsatisfiedWishesNumber([
      {
        game: defaultGame,
        slot: saturdayNightSlot,
        gm: null,
        players: [shadowPlayer],
      },
    ], [shadowPlayer])).toEqual(0);
  });

  it('should return the non-exauced wishes number if not', () => {
    const shadowPlayer = {
      ...nelimPlayer,
      wishes: [{ id: 2, level: WISH_LEVELS.WISH, xor: [] }, { id: 1, level: WISH_LEVELS.NEED, xor: [] }],
    };

    expect(getUnsatisfiedWishesNumber([
      {
        game: defaultGame,
        slot: saturdayNightSlot,
        gm: null,
        players: [shadowPlayer],
      },
    ], [shadowPlayer])).toEqual(1);
  });

  it('should ignore given level', () => {
    const shadowPlayer = {
      ...nelimPlayer,
      wishes: [{ id: 2, level: WISH_LEVELS.WISH, xor: [] }, { id: 1, level: WISH_LEVELS.NEED, xor: [] }],
    };

    expect(getUnsatisfiedWishesNumber([], [shadowPlayer], WISH_LEVELS.NEED)).toEqual(1);
  });
});
