import { getGameSlotsPossibilities, getSchedule as rawGetSchedule } from './getSchedule';
import { GameRequest, Logger, ScoreRandomizer, WISH_LEVELS } from './models';
import {
  defaultGame,
  game2,
  multiSlotGame,

  logger,
  firstPlayerRandom,
  lastPlayerRandom,

  nelimPlayer,
  kevPlayer,
  iniginPlayer,

  saturdayNightSlot,
  sundayEveningSlot,
  sundayNightSlot,
  mondayEveningSlot,
  mondayNightSlot,
} from './testFixtures';

describe('getSchedule', () => {
  function getSchedule(args: Partial<GameRequest> = {}, random?: ScoreRandomizer, localLogger?: Logger): unknown {
    return rawGetSchedule.call(null, {
      games: [defaultGame],
      slots: [saturdayNightSlot],
      players: [nelimPlayer],
      ...args,
    }, random ?? firstPlayerRandom, localLogger ?? logger);
  }

  describe('no game', () => {
    it('should return no game slot if none is available', async () => {
      expect(await getSchedule({
        slots: [],
      })).toEqual({
        results: [],
        leftGames: [defaultGame],
        leftSlots: [],
        leftPlayers: [nelimPlayer],
        leftPlayersBySlot: {},
      });
    });

    it('should return no game slot if no player is available', async () => {
      expect(await getSchedule({
        players: [],
      })).toEqual({
        results: [],
        leftGames: [defaultGame],
        leftSlots: [saturdayNightSlot],
        leftPlayers: [],
        leftPlayersBySlot: {},
      });
    });

    it('should return no game slot if no game is available', async () => {
      expect(await getSchedule({
        games: [],
      })).toEqual({
        results: [],
        leftGames: [],
        leftSlots: [saturdayNightSlot],
        leftPlayers: [nelimPlayer],
        leftPlayersBySlot: {},
      });
    });

    it('should return no game slot if not enough players are available', async () => {
      const game = {
        ...defaultGame,
        players: {
          minNumber: 2,
          maxNumber: defaultGame.players.maxNumber,
        },
      };

      expect(await getSchedule({
        games: [game],
        players: [nelimPlayer],
        slots: [saturdayNightSlot],
      })).toEqual({
        results: [],
        leftGames: [game],
        leftSlots: [saturdayNightSlot],
        leftPlayers: [],
        leftPlayersBySlot: {
          [saturdayNightSlot.id]: [nelimPlayer],
        },
      });
    });

    it('should return no game slot if not enough interested players are available', async () => {
      const noWishesNelimPlayer = {
        ...nelimPlayer,
        wishes: [],
      };

      expect(await getSchedule({
        games: [defaultGame],
        players: [noWishesNelimPlayer],
        slots: [saturdayNightSlot],
      })).toEqual({
        results: [],
        leftGames: [defaultGame],
        leftSlots: [saturdayNightSlot],
        leftPlayers: [],
        leftPlayersBySlot: {
          [saturdayNightSlot.id]: [noWishesNelimPlayer],
        },
      });
    });
  });

  it('should return a game slot if an interested player, a game with min number met and a slot are found', async () => {
    expect(await getSchedule()).toEqual({
      results: [{
        slot: saturdayNightSlot,
        game: defaultGame,
        gm: null,
        players: [nelimPlayer],
      }],
      leftGames: [],
      leftSlots: [],
      leftPlayers: [],
      leftPlayersBySlot: {},
    });
  });

  it('should return only interested players', async () => {
    const noWishesNelimPlayer = {
      ...nelimPlayer,
      id: 2,
      wishes: [],
    };

    expect(await getSchedule({
      players: [noWishesNelimPlayer, nelimPlayer ],
    })).toEqual({
      results: [{
        slot: saturdayNightSlot,
        game: defaultGame,
        gm: null,
        players: [nelimPlayer],
      }],
      leftGames: [],
      leftSlots: [],
      leftPlayers: [],
      leftPlayersBySlot: {
        [saturdayNightSlot.id]: [noWishesNelimPlayer],
      },
    });
  });

  it('should return multiple slots if given sorted by order', async () => {
    expect(await getSchedule({
      games: [defaultGame, game2],
      slots: [sundayEveningSlot, saturdayNightSlot],
    })).toEqual({
      results: [
        {
          slot: saturdayNightSlot,
          game: game2,
          gm: null,
          players: [nelimPlayer],
        },
        {
          slot: sundayEveningSlot,
          game: defaultGame,
          gm: null,
          players: [nelimPlayer],
        },
      ],
      leftGames: [],
      leftSlots: [],
      leftPlayers: [],
      leftPlayersBySlot: {},
    });
  });

  it('should not return a game multiple times if already taken by a slot', async () => {
    expect(await getSchedule({
      slots: [saturdayNightSlot, sundayEveningSlot],
    })).toEqual({
      results: [{
        slot: saturdayNightSlot,
        game: defaultGame,
        gm: null,
        players: [nelimPlayer],
      },
    ],
      leftGames: [],
      leftSlots: [sundayEveningSlot],
      leftPlayers: [],
      leftPlayersBySlot: {
        [sundayEveningSlot.id]: [nelimPlayer],
      },
    });
  });

  it('should ignore games if not enough players', async () => {
    const gameWithHighMin = {
      ...defaultGame,
      players: {
        minNumber: 1000,
        maxNumber: defaultGame.players.maxNumber,
      },
    };

    expect(await getSchedule({
      games: [gameWithHighMin],
      players: [nelimPlayer],
    })).toEqual({
      results: [],
      leftGames: [gameWithHighMin],
      leftSlots: [saturdayNightSlot],
      leftPlayers: [],
      leftPlayersBySlot: {
        [saturdayNightSlot.id]: [nelimPlayer],
      },
    });
  });

  it('should select the maximum number of randomly selected players depending on given random method', async () => {
    const game1 = {
      ...defaultGame,
      players: {
        minNumber: defaultGame.players.minNumber,
        maxNumber: 2,
      },
    };

    const game2bis = {
      ...game2,
      players: {
        minNumber: game2.players.minNumber,
        maxNumber: 2,
      },
    };

    expect(await getSchedule({
      games: [game1, game2],
      players: [nelimPlayer, kevPlayer, iniginPlayer],
    }, lastPlayerRandom)).toEqual({
      results: [
        {
          slot: saturdayNightSlot,
          game: game1,
          gm: null,
          players: [iniginPlayer, kevPlayer],
        },
        {
          slot: saturdayNightSlot,
          game: game2bis,
          gm: null,
          players: [nelimPlayer],
        },
      ],
      leftGames: [],
      leftSlots: [],
      leftPlayers: [],
      leftPlayersBySlot: {},
    });
  });

  describe('MORE NEED players', () => {
    it('should ignore games if not max NEED players available and others slots available', async () => {
      const gameWithNeeds = {
        ...defaultGame,
        wishes: [
          { id: 1, level: WISH_LEVELS.NEED, xor: [] },
          { id: 2, level: WISH_LEVELS.NEED, xor: [] },
          { id: 3, level: WISH_LEVELS.WISH, xor: [] },
          { id: 4, level: WISH_LEVELS.WISH, xor: [] },
        ],
        players: {
          minNumber: 3,
          maxNumber: 3,
        },
      };

      const shadowPlayer1 = {
        ...nelimPlayer,
        id: 3,
        wishes: [
          { id: 1, level: WISH_LEVELS.WISH, xor: [] },
        ],
      };

      const shadowPlayer2 = {
        ...nelimPlayer,
        id: 4,
        wishes: [
          { id: 1, level: WISH_LEVELS.WISH, xor: [] },
        ],
      };

      expect(await getSchedule({
        games: [gameWithNeeds],
        players: [nelimPlayer, shadowPlayer1, shadowPlayer2],
        slots: [saturdayNightSlot, sundayEveningSlot],
      })).toEqual({
        results: [],
        leftGames: [gameWithNeeds],
        leftSlots: [saturdayNightSlot, sundayEveningSlot],
        leftPlayers: [],
        leftPlayersBySlot: {
          [saturdayNightSlot.id]: [nelimPlayer, shadowPlayer1, shadowPlayer2],
          [sundayEveningSlot.id]: [nelimPlayer, shadowPlayer1, shadowPlayer2],
        },
      });
    });

    it('should not ignore games if more NEED players than possible in game', async () => {
      const gameWithNeeds = {
        ...defaultGame,
        wishes: [
          { id: 1, level: WISH_LEVELS.NEED, xor: [] },
          { id: 2, level: WISH_LEVELS.NEED, xor: [] },
          { id: 3, level: WISH_LEVELS.NEED, xor: [] },
          { id: 4, level: WISH_LEVELS.NEED, xor: [] },
        ],
        players: {
          minNumber: 3,
          maxNumber: 3,
        },
      };

      const shadowPlayer1 = {
        ...nelimPlayer,
        id: 3,
        wishes: [
          { id: 1, level: WISH_LEVELS.NEED, xor: [] },
        ],
      };

      const shadowPlayer2 = {
        ...nelimPlayer,
        id: 4,
        wishes: [
          { id: 1, level: WISH_LEVELS.NEED, xor: [] },
        ],
      };

      expect(await getSchedule({
        games: [gameWithNeeds],
        players: [nelimPlayer, shadowPlayer1, shadowPlayer2],
        slots: [saturdayNightSlot, sundayEveningSlot],
      })).toEqual({
        results: [{
          slot: saturdayNightSlot,
          players: [nelimPlayer, shadowPlayer1, shadowPlayer2],
          game: gameWithNeeds,
          gm: null,
        }],
        leftGames: [],
        leftSlots: [sundayEveningSlot],
        leftPlayers: [],
        leftPlayersBySlot: {
          [sundayEveningSlot.id]: [nelimPlayer, shadowPlayer1, shadowPlayer2],
        },
      });
    });

    it('should not ignore games if no more slot left', async () => {
      expect(await getSchedule({
        games: [defaultGame],
        players: [nelimPlayer],
        slots: [saturdayNightSlot],
      }, undefined, console)).toEqual({
        results: [{
          slot: saturdayNightSlot,
          players: [nelimPlayer],
          game: defaultGame,
          gm: null,
        }],
        leftGames: [],
        leftSlots: [],
        leftPlayers: [],
        leftPlayersBySlot: {},
      });
    });
  });

  it('should return multiple players if possible but not the same multiple times', async () => {
    const game1 = {
      ...defaultGame,
      players: {
        minNumber: defaultGame.players.minNumber,
        maxNumber: 2,
      },
    };

    expect(await getSchedule({
      games: [game1],
      players: [nelimPlayer, kevPlayer],
    })).toEqual({
      results: [
        {
          slot: saturdayNightSlot,
          game: game1,
          gm: null,
          players: [nelimPlayer, kevPlayer],
        },
      ],
      leftGames: [],
      leftSlots: [],
      leftPlayers: [],
      leftPlayersBySlot: {},
    });
  });

  describe('min and max', () => {
    it('should not return players more than max number waited', async () => {
      const game1 = {
        ...defaultGame,
        players: {
          minNumber: defaultGame.players.minNumber,
          maxNumber: 2,
        },
      };

      expect(await getSchedule({
        games: [game1],
        players: [nelimPlayer, kevPlayer, iniginPlayer],
      })).toEqual({
        results: [
          {
            slot: saturdayNightSlot,
            game: game1,
            gm: null,
            players: [nelimPlayer, kevPlayer],
          },
        ],
        leftGames: [],
        leftSlots: [],
        leftPlayers: [],
        leftPlayersBySlot: {
          [saturdayNightSlot.id]: [iniginPlayer],
        },
      });
    });

    it('should handle GM without min or max number', async () => {
      const gameWithGm = {
        ...defaultGame,
        gmId: 3,
        players: {
          minNumber: 2,
          maxNumber: 2,
        },
      };

      expect(await getSchedule({
        games: [gameWithGm],
        players: [iniginPlayer, nelimPlayer, kevPlayer],
      })).toEqual({
        results: [
          {
            slot: saturdayNightSlot,
            game: gameWithGm,
            gm: iniginPlayer,
            players: [nelimPlayer, kevPlayer],
          },
        ],
        leftGames: [],
        leftSlots: [],
        leftPlayers: [],
        leftPlayersBySlot: {},
      });
    });
  });

  describe('gm', () => {
    it('should handle GM even if not tagged as interested', async () => {
      const gameWithGm = {
        ...defaultGame,
        gmId: 3,
        players: {
          minNumber: 2,
          maxNumber: 2,
        },
      };

      const shadowPlayer = {
        ...iniginPlayer,
        wishes: [],
      };

      expect(await getSchedule({
        games: [gameWithGm],
        players: [shadowPlayer, nelimPlayer, kevPlayer],
      })).toEqual({
        results: [
          {
            slot: saturdayNightSlot,
            game: gameWithGm,
            gm: shadowPlayer,
            players: [nelimPlayer, kevPlayer],
          },
        ],
        leftGames: [],
        leftSlots: [],
        leftPlayers: [],
        leftPlayersBySlot: {},
      });
    });

    it('should handle GM for min number', async () => {
      const gameWithGm = {
        ...defaultGame,
        gmId: 2,
        players: {
          minNumber: 3,
          maxNumber: 1000,
        },
      };

      expect(await getSchedule({
        games: [gameWithGm],
        players: [nelimPlayer, kevPlayer, iniginPlayer],
      })).toEqual({
        results: [],
        leftGames: [gameWithGm],
        leftSlots: [saturdayNightSlot],
        leftPlayers: [],
        leftPlayersBySlot: {
          [saturdayNightSlot.id]: [
            nelimPlayer, kevPlayer, iniginPlayer
          ],
        },
      });
    });

    it('should handle GM for max number', async () => {
      const gameWithGm = {
        ...defaultGame,
        gmId: 3,
        players: {
          minNumber: 1,
          maxNumber: 1,
        },
      };

      expect(await getSchedule({
        games: [gameWithGm],
        players: [nelimPlayer, kevPlayer, iniginPlayer],
      })).toEqual({
        results: [
          {
            slot: saturdayNightSlot,
            game: gameWithGm,
            gm: iniginPlayer,
            players: [nelimPlayer],
          },
        ],
        leftGames: [],
        leftSlots: [],
        leftPlayers: [],
        leftPlayersBySlot: {
          [saturdayNightSlot.id]: [
            kevPlayer
          ],
        },
      });
    });

    it('should ignore games with already playing GM (GM must be available for a game to start)', async () => {
      const gameWithGm = {
        ...defaultGame,
        id: 2,
        gmId: 3,
        players: {
          minNumber: 1,
          maxNumber: 1,
        },
      };

      expect(await getSchedule({
        games: [defaultGame, gameWithGm],
        players: [iniginPlayer, nelimPlayer, kevPlayer],
      })).toEqual({
        results: [
          {
            slot: saturdayNightSlot,
            game: defaultGame,
            gm: null,
            players: [iniginPlayer],
          },
        ],
        leftGames: [gameWithGm],
        leftSlots: [],
        leftPlayers: [],
        leftPlayersBySlot: {
          [saturdayNightSlot.id]: [nelimPlayer, kevPlayer],
        },
      });
    });

    it('should ignore games with GM having already playing the same day if all slots can be placed', async () => {
      const gameWithGm = {
        ...defaultGame,
        id: 2,
        gmId: 3,
      };
      const gameWithGmBeforeTheSameDay = {
        ...defaultGame,
        gmId: 3,
      };

      expect(await getSchedule({
        games: [gameWithGmBeforeTheSameDay, gameWithGm],
        players: [iniginPlayer, nelimPlayer],
        slots: [sundayEveningSlot, sundayNightSlot, mondayEveningSlot],
      })).toEqual({
        results: [
          {
            slot: sundayEveningSlot,
            game: gameWithGmBeforeTheSameDay,
            gm: iniginPlayer,
            players: [nelimPlayer],
          },
          {
            slot: mondayEveningSlot,
            game: gameWithGm,
            gm: iniginPlayer,
            players: [nelimPlayer],
          },
        ],
        leftGames: [],
        leftSlots: [sundayNightSlot],
        leftPlayers: [],
        leftPlayersBySlot: {
          [sundayNightSlot.id]: [iniginPlayer, nelimPlayer],
        },
      });

      expect(await getSchedule({
        games: [gameWithGmBeforeTheSameDay, gameWithGm],
        players: [iniginPlayer, nelimPlayer],
        slots: [sundayNightSlot, sundayEveningSlot, mondayEveningSlot],
      })).toEqual({
        results: [
          {
            slot: sundayNightSlot,
            game: gameWithGmBeforeTheSameDay,
            gm: iniginPlayer,
            players: [nelimPlayer],
          },
          {
            slot: mondayEveningSlot,
            game: gameWithGm,
            gm: iniginPlayer,
            players: [nelimPlayer],
          },
        ],
        leftGames: [],
        leftSlots: [sundayEveningSlot],
        leftPlayers: [],
        leftPlayersBySlot: {
          [sundayEveningSlot.id]: [iniginPlayer, nelimPlayer],
        },
      });
    });

    it('should not ignore games with GM having already playing the same day if all slots cannot be placed', async () => {
      const gameWithGm = {
        ...defaultGame,
        id: 2,
        gmId: 3,
      };
      const gameWithGmBeforeTheSameDay = {
        ...defaultGame,
        gmId: 3,
      };

      expect(await getSchedule({
        games: [gameWithGmBeforeTheSameDay, gameWithGm],
        players: [iniginPlayer, nelimPlayer],
        slots: [sundayEveningSlot, sundayNightSlot],
      })).toEqual({
        results: [
          {
            slot: sundayEveningSlot,
            game: gameWithGmBeforeTheSameDay,
            gm: iniginPlayer,
            players: [nelimPlayer],
          },
          {
            slot: sundayNightSlot,
            game: gameWithGm,
            gm: iniginPlayer,
            players: [nelimPlayer],
          },
        ],
        leftGames: [],
        leftSlots: [],
        leftPlayers: [],
        leftPlayersBySlot: {},
      });
    });
  });

  describe('multi-slot games', () => {
    it('should return no game slot if not enough slots are available', async () => {
      expect(await getSchedule({
        games: [multiSlotGame],
        slots: [saturdayNightSlot],
      })).toEqual({
        results: [],
        leftGames: [multiSlotGame],
        leftSlots: [saturdayNightSlot],
        leftPlayers: [],
        leftPlayersBySlot: {
          [saturdayNightSlot.id]: [nelimPlayer],
        },
      });
    });

    it('should return no game slot if no player is available', async () => {
      expect(await getSchedule({
        players: [],
        slots: [saturdayNightSlot, sundayEveningSlot],
      })).toEqual({
        results: [],
        leftGames: [defaultGame],
        leftSlots: [saturdayNightSlot, sundayEveningSlot],
        leftPlayers: [],
        leftPlayersBySlot: {},
      });
    });

    it('should return no game slot if players are not available on all slots', async () => {
      const game = {
        ...defaultGame,
        sessions: {
          minNumber: 2,
          maxNumber: 2,
        },
      };

      expect(await getSchedule({
        games: [game2, game],
        players: [nelimPlayer],
        slots: [sundayEveningSlot, sundayNightSlot],
      })).toEqual({
        results: [{
          slot: sundayEveningSlot,
          game: game2,
          gm: null,
          players: [nelimPlayer],
        }],
        leftGames: [game],
        leftSlots: [sundayNightSlot],
        leftPlayers: [],
        leftPlayersBySlot: {
          [sundayNightSlot.id]: [nelimPlayer],
        },
      });
    });

    it('should return multiple game slots if an interested player, a multi-slot game with all min numbers met and slots are found', async () => {
      expect(await getSchedule({
        games: [multiSlotGame],
        slots: [saturdayNightSlot, sundayEveningSlot],
      })).toEqual({
        results: [{
          slot: saturdayNightSlot,
          game: multiSlotGame,
          gm: null,
          players: [nelimPlayer],
        },
        {
          slot: sundayEveningSlot,
          game: multiSlotGame,
          gm: null,
          players: [nelimPlayer],
        }],
        leftGames: [],
        leftSlots: [],
        leftPlayers: [],
        leftPlayersBySlot: {},
      });
    });

    it('should return multiple slots if given sorted by order', async () => {
      expect(await getSchedule({
        games: [defaultGame, game2, multiSlotGame],
        slots: [saturdayNightSlot, sundayEveningSlot, sundayNightSlot, mondayEveningSlot],
      })).toEqual({
        results: [
          {
            slot: saturdayNightSlot,
            game: defaultGame,
            gm: null,
            players: [nelimPlayer],
          },
          {
            slot: sundayEveningSlot,
            game: game2,
            gm: null,
            players: [nelimPlayer],
          },
          {
            slot: sundayNightSlot,
            game: multiSlotGame,
            gm: null,
            players: [nelimPlayer],
          },
          {
            slot: mondayEveningSlot,
            game: multiSlotGame,
            gm: null,
            players: [nelimPlayer],
          },
        ],
        leftGames: [],
        leftSlots: [],
        leftPlayers: [],
        leftPlayersBySlot: {},
      });
    });

    it('should return no game slot if GM is not available', async () => {
      const gameWithGm = {
        ...defaultGame,
        gmId: 3,
        players: {
          minNumber: 2,
          maxNumber: 2,
        },
      };
      const multiSlotGameWithGm = {
        ...multiSlotGame,
        gmId: 3,
        players: {
          minNumber: 2,
          maxNumber: 2,
        },
      }

      expect(await getSchedule({
        games: [gameWithGm, multiSlotGameWithGm],
        players: [iniginPlayer, nelimPlayer, kevPlayer],
      })).toEqual({
        results: [
          {
            slot: saturdayNightSlot,
            game: gameWithGm,
            gm: iniginPlayer,
            players: [nelimPlayer, kevPlayer],
          },
        ],
        leftGames: [multiSlotGameWithGm],
        leftSlots: [],
        leftPlayers: [],
        leftPlayersBySlot: {},
      });
    });

    it('should return multi game with GM too', async () => {
      const gameWithGm = {
        ...defaultGame,
        gmId: 3,
        players: {
          minNumber: 2,
          maxNumber: 2,
        },
      };
      const multiSlotGameWithGm = {
        ...multiSlotGame,
        gmId: 3,
        players: {
          minNumber: 2,
          maxNumber: 2,
        },
      };

      expect(await getSchedule({
        games: [gameWithGm, multiSlotGameWithGm],
        players: [iniginPlayer, nelimPlayer, kevPlayer],
        slots: [saturdayNightSlot, sundayEveningSlot, sundayNightSlot],
      })).toEqual({
        results: [
          {
            slot: saturdayNightSlot,
            game: gameWithGm,
            gm: iniginPlayer,
            players: [nelimPlayer, kevPlayer],
          },
          {
            slot: sundayEveningSlot,
            game: multiSlotGameWithGm,
            gm: iniginPlayer,
            players: [nelimPlayer, kevPlayer],
          },
          {
            slot: sundayNightSlot,
            game: multiSlotGameWithGm,
            gm: iniginPlayer,
            players: [nelimPlayer, kevPlayer],
          },
        ],
        leftGames: [],
        leftSlots: [],
        leftPlayers: [],
        leftPlayersBySlot: {},
      });
    });

    it('should not consider GM as available if already GMing another slot', async () => {
      const multiSlotGameWithGm = { // should be for inigin and kev over saturday & sunday
        ...multiSlotGame,
        id: 3,
        gmId: 3,
        players: {
          minNumber: 1,
          maxNumber: 1,
        },
      };
      const gameForOne = { // only nelim over saturday
        ...game2,
        id: 2,
        players: {
          minNumber: 1,
          maxNumber: 3,
        },
      };
      const gameForThree = { // nelim is alone here on sunday
        ...game2,
        id: 1,
        name: 'Game 3',
        players: {
          minNumber: 1,
          maxNumber: 3,
        },
      };

      expect(await getSchedule({
        games: [multiSlotGameWithGm, gameForOne, gameForThree],
        players: [iniginPlayer, kevPlayer, nelimPlayer],
        slots: [saturdayNightSlot, sundayEveningSlot],
      })).toEqual({
        results: [
          {
            slot: saturdayNightSlot,
            game: multiSlotGameWithGm,
            gm: iniginPlayer,
            players: [kevPlayer],
          },
          {
            slot: saturdayNightSlot,
            game: gameForOne,
            gm: null,
            players: [nelimPlayer],
          },
          {
            slot: sundayEveningSlot,
            game: multiSlotGameWithGm,
            gm: iniginPlayer,
            players: [kevPlayer],
          },
          {
            slot: sundayEveningSlot,
            game: gameForThree,
            gm: null,
            players: [nelimPlayer],
          },
        ],
        leftGames: [],
        leftSlots: [],
        leftPlayers: [],
        leftPlayersBySlot: {},
      });
    });

    it('should ignore games if not max NEED players available and enough others slots available', async () => {
      const gameWithNeeds = {
        ...multiSlotGame,
        wishes: [
          { id: 1, level: WISH_LEVELS.NEED, xor: [] },
          { id: 2, level: WISH_LEVELS.NEED, xor: [] },
          { id: 3, level: WISH_LEVELS.WISH, xor: [] },
          { id: 4, level: WISH_LEVELS.WISH, xor: [] },
        ],
        players: {
          minNumber: 3,
          maxNumber: 3,
        },
      };

      const shadowPlayer1 = {
        ...nelimPlayer,
        id: 3,
        wishes: [
          { id: 1, level: WISH_LEVELS.WISH, xor: [] },
        ],
      };

      const shadowPlayer2 = {
        ...nelimPlayer,
        id: 4,
        wishes: [
          { id: 1, level: WISH_LEVELS.WISH, xor: [] },
        ],
      };

      expect(await getSchedule({
        games: [gameWithNeeds],
        players: [nelimPlayer, shadowPlayer1, shadowPlayer2],
        slots: [saturdayNightSlot, sundayEveningSlot, mondayEveningSlot, mondayNightSlot],
      })).toEqual({
        results: [],
        leftGames: [gameWithNeeds],
        leftSlots: [saturdayNightSlot, sundayEveningSlot, mondayEveningSlot, mondayNightSlot],
        leftPlayers: [],
        leftPlayersBySlot: {
          [saturdayNightSlot.id]: [nelimPlayer, shadowPlayer1, shadowPlayer2],
          [sundayEveningSlot.id]: [nelimPlayer, shadowPlayer1, shadowPlayer2],
          [mondayEveningSlot.id]: [nelimPlayer, shadowPlayer1, shadowPlayer2],
          [mondayNightSlot.id]: [nelimPlayer, shadowPlayer1, shadowPlayer2],
        },
      });
    });
  });

  describe('xor', () => {
    it('should not place the player to xor games if already one selected (2 xors)', async () => {
      const xorPlayer = {
        ...nelimPlayer,
        wishes: [{ id: 1, level: WISH_LEVELS.NEED, xor: [2] }, { id: 2, level: WISH_LEVELS.NEED, xor: [1] }],
      };
      expect(await getSchedule({
        games: [defaultGame, game2],
        players: [xorPlayer],
        slots: [saturdayNightSlot, sundayEveningSlot],
      })).toEqual({
        results: [
          {
            slot: saturdayNightSlot,
            game: defaultGame,
            gm: null,
            players: [xorPlayer],
          },
        ],
        leftGames: [game2],
        leftSlots: [sundayEveningSlot],
        leftPlayers: [],
        leftPlayersBySlot: {
          [sundayEveningSlot.id]: [xorPlayer],
        },
      });
    });

    it('should not place the player to xor games if already one selected (3 xors)', async () => {
      const xorPlayer = {
        ...nelimPlayer,
        wishes: [
          { id: 1, level: WISH_LEVELS.NEED, xor: [2, 3] },
          { id: 2, level: WISH_LEVELS.NEED, xor: [1, 3] },
          { id: 3, level: WISH_LEVELS.NEED, xor: [1, 2] },
        ],
      };

      const game3 = {
        ...defaultGame,
        id: 3,
      };

      expect(await getSchedule({
        games: [defaultGame, game2, game3],
        players: [xorPlayer],
        slots: [saturdayNightSlot, sundayEveningSlot],
      })).toEqual({
        results: [
          {
            slot: saturdayNightSlot,
            game: defaultGame,
            gm: null,
            players: [xorPlayer],
          },
        ],
        leftGames: [game2, game3],
        leftSlots: [sundayEveningSlot],
        leftPlayers: [],
        leftPlayersBySlot: {
          [sundayEveningSlot.id]: [xorPlayer],
        },
      });
    });
  });
});

describe('getGameSlotsPossibilities', () => {
  const allSlots = [saturdayNightSlot, sundayEveningSlot, sundayNightSlot, mondayEveningSlot, mondayNightSlot];

  it('should return one slot possibility if only one gameSlot required', () => {
    expect(getGameSlotsPossibilities(allSlots, saturdayNightSlot, 1, logger)).toEqual([
      {
        gameSlots: [saturdayNightSlot],
        isOk: null,
      },
    ]);
  });

  describe('if 2 game slots required', () => {
    it('should return two slots possibilities starting by call slot', () => {
      expect(getGameSlotsPossibilities(allSlots, sundayEveningSlot, 2, logger)).toEqual([
        {
          gameSlots: [sundayEveningSlot, saturdayNightSlot],
          isOk: null,
        },
        {
          gameSlots: [sundayEveningSlot, sundayNightSlot],
          isOk: null,
        },
      ]);
    });

    it('should return one possible slots possibility only if one is unavailable because nothing before', () => {
      expect(getGameSlotsPossibilities(allSlots, saturdayNightSlot, 2, logger)).toEqual([
        {
          gameSlots: [saturdayNightSlot, sundayEveningSlot],
          isOk: null,
        },
      ]);
    });

    it('should return one possible slots possibility only if one is unavailable because nothing after', () => {
      expect(getGameSlotsPossibilities(allSlots, mondayNightSlot, 2, logger)).toEqual([
        {
          gameSlots: [mondayNightSlot, mondayEveningSlot],
          isOk: null,
        },
      ]);
    });
  });

  describe('3 slots', () => {
    it('should return three slots possibilities if three gameSlot available', () => {
      expect(getGameSlotsPossibilities(allSlots, sundayNightSlot, 3, logger)).toEqual([
        {
          gameSlots: [sundayNightSlot, saturdayNightSlot, sundayEveningSlot],
          isOk: null,
        },
        {
          gameSlots: [sundayNightSlot, sundayEveningSlot, mondayEveningSlot],
          isOk: null,
        },
        {
          gameSlots: [sundayNightSlot, mondayEveningSlot, mondayNightSlot],
          isOk: null,
        },
      ]);
    });

    it('should return two possible slots possibility only if one is unavailable because nothing before', () => {
      expect(getGameSlotsPossibilities(allSlots, sundayEveningSlot, 3, logger)).toEqual([
        {
          gameSlots: [sundayEveningSlot, saturdayNightSlot, sundayNightSlot],
          isOk: null,
        },
        {
          gameSlots: [sundayEveningSlot, sundayNightSlot, mondayEveningSlot],
          isOk: null,
        },
      ]);
    });

    it('should return one possible slots possibility only if two are unavailable because nothing before', () => {
      expect(getGameSlotsPossibilities(allSlots, saturdayNightSlot, 3, logger)).toEqual([
        {
          gameSlots: [saturdayNightSlot, sundayEveningSlot, sundayNightSlot],
          isOk: null,
        },
      ]);
    });

    it('should return two possible slots possibility only if one is unavailable because nothing after', () => {
      expect(getGameSlotsPossibilities(allSlots, mondayEveningSlot, 3, logger)).toEqual([
        {
          gameSlots: [mondayEveningSlot, sundayEveningSlot, sundayNightSlot],
          isOk: null,
        },
        {
          gameSlots: [mondayEveningSlot, sundayNightSlot, mondayNightSlot],
          isOk: null,
        },
      ]);
    });

    it('should return one possible slots possibility only if two are unavailable because nothing after', () => {
      expect(getGameSlotsPossibilities(allSlots, mondayNightSlot, 3, logger)).toEqual([
        {
          gameSlots: [mondayNightSlot, sundayNightSlot, mondayEveningSlot],
          isOk: null,
        },
      ]);
    });
  });
});
