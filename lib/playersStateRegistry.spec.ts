import { PlayerStateRegistry, WISH_LEVELS } from './models';
import { getScoreByPlayerState } from './playersStateRegistry';
import { defaultGame, kevPlayer, nelimPlayer, saturdayNightSlot } from './testFixtures';

describe('playersStateRegistry', () => {
  describe('getScoreByPlayerState', () => {
    it('should return a NEED player (for the given game) first if possible', () => {
      const shadowPlayer = {
        ...nelimPlayer,
        id: 2,
        wishes: [{ id: 1, level: WISH_LEVELS.WISH, xor: [] }, { id: 2, level: WISH_LEVELS.NEED, xor: [] }],
      };
      expect(getScoreByPlayerState([
        nelimPlayer,
        shadowPlayer,
      ], new PlayerStateRegistry([nelimPlayer, shadowPlayer]), saturdayNightSlot, defaultGame)).toEqual({
        previousScore: 0,
        diff: expect.any(Number),
        score: expect.any(Number),
        player: nelimPlayer,
      });
    });

    it('should return the player that have played the less', () => {
      const playerRegistry = new PlayerStateRegistry([nelimPlayer, kevPlayer]);
      --playerRegistry.get(kevPlayer).played;
      expect(getScoreByPlayerState([
        nelimPlayer,
        kevPlayer,
      ], playerRegistry, saturdayNightSlot, defaultGame)).toEqual({
        previousScore: expect.any(Number),
        diff: expect.any(Number),
        score: expect.any(Number),
        player: kevPlayer,
      });
    });

    it('should return a player that have mastered if possible', () => {
      const playerRegistry = new PlayerStateRegistry([nelimPlayer, kevPlayer]);
      ++playerRegistry.get(kevPlayer).gmOrNot;
      expect(getScoreByPlayerState([
        nelimPlayer,
        kevPlayer,
      ], playerRegistry, saturdayNightSlot, defaultGame)).toEqual({
        previousScore: expect.any(Number),
        diff: expect.any(Number),
        score: expect.any(Number),
        player: kevPlayer,
      });
    });

    it('should return a player that have mainly played in the opposed part of the week', () => {
      const playerRegistry = new PlayerStateRegistry([nelimPlayer, kevPlayer]);
      playerRegistry.get(kevPlayer).weekPosition = 7;
      expect(getScoreByPlayerState([
        nelimPlayer,
        kevPlayer,
      ], playerRegistry, saturdayNightSlot, defaultGame)).toEqual({
        previousScore: expect.any(Number),
        diff: expect.any(Number),
        score: expect.any(Number),
        player: kevPlayer,
      });
    });

    it('should return a player that have mainly played in the opposed part of the day', () => {
      const playerRegistry = new PlayerStateRegistry([nelimPlayer, kevPlayer]);
      playerRegistry.get(kevPlayer).dayPosition = -3; // three evenings
      expect(getScoreByPlayerState([
        nelimPlayer,
        kevPlayer,
      ], playerRegistry, saturdayNightSlot, defaultGame)).toEqual({
        previousScore: expect.any(Number),
        diff: expect.any(Number),
        score: expect.any(Number),
        player: kevPlayer,
      });
    });
  });
});
