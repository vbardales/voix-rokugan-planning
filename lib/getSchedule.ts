import * as _ from 'lodash';
import {
  GameRequest,
  GameSchedule,
  Player,
  PlayerStateRegistry,
  Slot,
  GameResult,
  Game,
  Possibility,
  Logger,
  Wish,
  ScoreRandomizer,
  DAY_POSITION,
} from './models';
import { updatePlayersStateRegistry } from './playersStateRegistry';

export async function getSchedule({
  games,
  slots,
  players,
 }: GameRequest, random: ScoreRandomizer, logger: Logger): Promise<GameSchedule> {
  logger.info('READY TO BEGIN');
  logger.info(`Found ${slots.length} slot(s), ${games.length} game(s), ${players.length} player(s)`);

  if (!games.length || !players.length || !slots.length) {
    logger.warn('Not enough games, players or slots found');
    return { results: [], leftGames: games, leftSlots: slots, leftPlayers: players, leftPlayersBySlot: {} };
  }

  const leftGames = [...games];
  const leftSlots: Slot[] = [...slots];

  const results: GameResult[] = [];
  const leftPlayersBySlot: Record<string, Player[]> = {};
  const playerStateRegistry: PlayerStateRegistry = new PlayerStateRegistry(players);

  for (const slot of slots) {
    leftPlayersBySlot[slot.id] = [...players];
  }

  // slots are ordered and random
  for (const slot of slots) {
    let foundResult = false;
    // games are ordered and random
    const slotGames = [...leftGames];
    for (const game of slotGames) {
      let playersForGame;
      try {
        // players are ordered and random
        playersForGame = checkEnoughPlayers(game, leftPlayersBySlot[slot.id], leftSlots.length, results.map(result => result.game.id));
      } catch (error) {
        logger.warn(`Not enough (NEED) players found (${error.data.length}) for game ${game.name} on slot ${slot.id}`);
        continue;
      }
      logger.debug(`Found ${playersForGame.length} interested player(s) for game ${game.name} on slot ${slot.id}`);

      const possibleGameSlots = getGameSlotsPossibilities(slots, slot, game.sessions.maxNumber, logger);
      const allPossibilities = [...possibleGameSlots];
      if (!possibleGameSlots.length) {
        logger.warn(`Not enough game slots found for game ${game.name} on slot ${slot.id}`);
        continue;
      }

      let gm: Player | null = null;
      const gamePlayers: Player[] = [];
      do {
        const currentPossibility = possibleGameSlots[0];
        logger.debug(`Trying possibility ${currentPossibility.toString()} on game ${game.name}`);

        if (game.gmId) {
          // exploration part
          try {
            gm = checkGmAvailable(game, leftPlayersBySlot[slot.id], null);
            const leftGamesAsGm = leftGames.filter((game) => game.gmId === gm?.id).length - 1;
            const leftSlotsAsGm = leftSlots.length - 1;
            const adjacentSlotId = slot.dayPosition === DAY_POSITION.EVENING ? slot.next : slot.prev;
            if (leftGamesAsGm < leftSlotsAsGm && adjacentSlotId) {
              const adjacentSlot = slots.find(slot => adjacentSlotId === slot.id);
              const foundResult = results.find(result => result.slot === adjacentSlot && result.game.gmId === gm?.id);
              if (foundResult) {
                throw new Error('GM is unavailable: has already mastered this day');
              }
            }
          } catch (error) {
            logger.warn(`GM ${game.gmId} is no more available for game ${game.name} on slot ${slot.id}`);
            currentPossibility.isOk = false;
            possibleGameSlots.splice(0, 1);
            continue;
          }

          for (const gameSlot of currentPossibility.gameSlots) {
            if (gameSlot === slot) {
              continue;
            }

            try {
              checkGmAvailable(game, leftPlayersBySlot[gameSlot.id], gm);
            } catch (error) {
              logger.warn(`GM ${game.gmId} is no more available for game ${game.name} on slot ${gameSlot.id}`);
              currentPossibility.isOk = false;
              break;
            }
          }

          if (currentPossibility.isOk === false) {
            possibleGameSlots.splice(0, 1);
            continue;
          }
          logger.debug(`Found GM ${gm.pseudo} for game ${game.name} on slot(s) ${currentPossibility.toString()}`);
        }

        const playersNumber = Math.min(playersForGame.length, game.players.maxNumber);
        logger.debug(`Searching for ${playersNumber} player(s) for game ${game.name} on slot(s) ${currentPossibility.toString()}`);
        const unavailablePlayers: Player[] = [];
        while (unavailablePlayers.length + gamePlayers.length < playersForGame.length && gamePlayers.length < playersNumber) {
          // select player (based on availability)
          const score = random.getScore(
            playersForGame.filter((playerToFilter) => !gamePlayers.includes(playerToFilter) && !unavailablePlayers.includes(playerToFilter)),
            playerStateRegistry,
            slot,
            game
          );
          const player = score.player;

          if (currentPossibility.gameSlots.length > 1) {
            for (const gameSlot of currentPossibility.gameSlots) {
              if (gameSlot === slot) {
                continue;
              }

              try {
                checkPlayerAvailable(player, leftPlayersBySlot[gameSlot.id]);
              } catch (error) {
                logger.warn(`Player ${player.pseudo} is no more available for game ${game.name} on slot ${gameSlot.id}`);
                unavailablePlayers.push(player);
                break;
              }
            }
          }

          if (unavailablePlayers.includes(player)) {
            continue;
          }

          // assign it to game
          gamePlayers.push(player);
          logger.debug(`Found player ${player.pseudo} for game ${game.name} on slot ${slot.id}`);
        }

        if (gamePlayers.length < playersNumber) {
          currentPossibility.isOk = false;
          possibleGameSlots.splice(0, 1);
          continue;
        }

        currentPossibility.isOk = true;
      } while (possibleGameSlots.length && possibleGameSlots[0].isOk !== true);

      const currentPossibility = possibleGameSlots[0];
      if (!currentPossibility?.isOk) {
        logger.warn(`No valid slot found for game ${game.name} on possibilities ${allPossibilities.map(possibility => possibility?.toString()).join(', ')}`);
        continue;
      }

      // writing part
      for (const gameSlot of currentPossibility.gameSlots) {
        if (gm) {
          // remove her from current players pool
          gm = leftPlayersBySlot[gameSlot.id].splice(leftPlayersBySlot[gameSlot.id].findIndex((playerToFind) => playerToFind === gm), 1)[0];
          // remove her from current game players if assigned as interested (can be possible)
          const gameGmIndex = playersForGame.findIndex(playerToFind => playerToFind === gm);
          if (gameGmIndex !== -1) {
            playersForGame.splice(gameGmIndex, 1);
          }
          // update her state
          updatePlayersStateRegistry(playerStateRegistry, gm, slot, game);
          logger.debug(`Removed GM ${gm.pseudo} from slot ${gameSlot.id} pool`);
        }

        gamePlayers.forEach((gamePlayer) => {
          // remove her from current players pool
          const playerIndex = leftPlayersBySlot[gameSlot.id].findIndex(playerToFind => playerToFind === gamePlayer);
          leftPlayersBySlot[gameSlot.id].splice(playerIndex, 1);
          // update her state
          updatePlayersStateRegistry(playerStateRegistry, gamePlayer, slot, game);
          logger.debug(`Removed player ${gamePlayer.pseudo} from slot ${gameSlot.id} pool`);
        });
      }

      for (const gameSlot of currentPossibility.gameSlots) {
        const result = {
          slot: gameSlot,
          game,
          gm,
          players: gamePlayers,
        };
        results.push(result);
      }
      logger.info(`Found game ${game.name} for slot(s) ${currentPossibility.toString()}, taken ${gamePlayers.length} player(s)`);
      foundResult = true;

      logger.info(`Removed game ${game.name} from games pool`);
      const foundGameIndex = leftGames.findIndex(gameToFind => gameToFind === game);
      leftGames.splice(foundGameIndex, 1);
    }

    if (!foundResult && !results.find((result) => result.slot === slot)) {
      logger.warn(`No game (with enough players) found for slot ${slot.id} (${leftPlayersBySlot[slot.id].length} player(s) left)`);
      continue;
    }

    logger.info(`Removed slot ${slot.id} from slots pool`);
    const foundSlotIndex = leftSlots.findIndex(slotToFind => slotToFind === slot);
    leftSlots.splice(foundSlotIndex, 1);
  }

  return {
    results: results
      .sort((a: GameResult, b: GameResult) => a.slot.order >= b.slot.order ? 1 : -1),
    leftGames,
    leftSlots,
    leftPlayers: [],
    leftPlayersBySlot: Object.keys(leftPlayersBySlot).reduce((acc: Record<string, Player[]>, slotId) => {
      if (leftPlayersBySlot[slotId].length) {
        acc[slotId] = leftPlayersBySlot[slotId];
      }
      return acc;
    }, {}),
  };
}

function checkEnoughPlayers(game: Game, leftPlayersForSlot: Player[], leftSlotsNumber: number, alreadySetGames: number[]): Player[] {
  const playersForGame = leftPlayersForSlot.filter((player) => player.wishes.find((wish) => wish.id === game.id && !_.intersection(wish.xor, alreadySetGames).length) && game.gmId !== player.id);

  if (playersForGame.length < game.players.minNumber) {
    const error = new ErrorWithData();
    error.data = playersForGame;
    throw error;
  }

  const needPlayersForGame = leftPlayersForSlot.filter((player) => Wish.getNeedWishIds(player.wishes).find((wish) => wish === game.id) && game.gmId !== player.id);
  if (leftSlotsNumber > game.sessions.maxNumber &&
      needPlayersForGame.length < Math.min(game.players.minNumber, Wish.getNeedWishIds(game.wishes).length)) {
    const error = new ErrorWithData();
    error.data = playersForGame;
    throw error;
  }

  return playersForGame;
}

class ErrorWithData extends Error {
  public data?: unknown;
}

function checkGmAvailable(game: Game, leftPlayersForSlot: Player[], gm: Player | null): Player {
  const gmFound = leftPlayersForSlot.find(playerToFind => playerToFind.id === (gm ? gm.id : game.gmId));
  if (!gmFound) {
    throw new Error();
  }
  return gm ?? gmFound;
}

function checkPlayerAvailable(player: Player, leftPlayersForSlot: Player[]): void {
  if (!leftPlayersForSlot.find(playerToFind => playerToFind.id === player.id)) {
    throw new Error();
  }
}

export function getGameSlotsPossibilities(slots: Slot[], originalSlot: Slot, sessionNumber: number, logger: Logger): Possibility[] {
  if (sessionNumber === 1) {
    return [
      new Possibility(originalSlot, [originalSlot]),
    ];
  }

  logger.info(`Center slot is ${originalSlot.id}`);
  const possibleGameSlots = [];
  for (let i = -1 * sessionNumber + 1; i <= sessionNumber - 2; ++i) {
    let firstSlot: Slot | null = originalSlot;
    let firstSlotIndex = 0;
    while (firstSlotIndex !== i && firstSlot !== null) {
      logger.debug(`Previous ${i}: Try with slot ${firstSlotIndex} - ${firstSlot.id} to find ${firstSlot.prev}`);
      firstSlot = firstSlot ? slots.find((slotToFind) => slotToFind.id === firstSlot?.prev) ?? null : null;
      --firstSlotIndex;
    }

    if (firstSlot === null) {
      logger.debug(`Previous ${i}: No slot before ${firstSlotIndex}, aborting`);
      continue;
    }

    let lastSlot: Slot | null = originalSlot;
    let lastSlotIndex = 0;
    while (lastSlotIndex !== i + sessionNumber - 1 && lastSlot !== null) {
      logger.debug(`Next ${i}: Try with slot ${lastSlotIndex} - ${lastSlot.id} to find ${lastSlot.next}`);
      lastSlot = lastSlot ? slots.find((slotToFind) => slotToFind.id === lastSlot?.next) ?? null : null;
      ++lastSlotIndex;
    }

    if (lastSlot === null) {
      logger.debug(`Next ${i}: No slot after ${lastSlotIndex}, aborting`);
      continue;
    }

    logger.debug(`Slots ${i} are all available, trying to reconstruct the possibility`);
    const possibleSlots: Slot[] = [];
    let currentSlot: Slot | null = firstSlot;
    do {
      if (currentSlot === null) {
        throw new Error('Should not be possible: slot is null');
      }

      possibleSlots.push(currentSlot);
      currentSlot = slots.find((slotToFind) => slotToFind.id === currentSlot?.next) ?? null;
    } while (currentSlot !== lastSlot);
    possibleSlots.push(lastSlot);

    const possibility = new Possibility(originalSlot, possibleSlots);
    logger.info(`Possibility ${possibility.toString()} found`);
    possibleGameSlots.push(possibility);
  }

  return possibleGameSlots;
}
