import { Worker } from 'worker_threads';
import { GameRequest, GameSchedule, Logger, ScoreRandomizer } from './models';

export function startAnalyze(request: GameRequest, name: string, random: ScoreRandomizer, logger: Logger): Promise<GameSchedule> {
  return new Promise((resolve, reject) => {
    const worker = new Worker('./lib/getScheduleWorker.js', {
      workerData: {
        players: request.players,
        slots: request.slots,
        games: request.games,
        randomName: random.name,
        loggerName: logger.name,
      },
    });

    worker.on('online', () => {
      logger.debug(`Starting analyzing ${name}`);
    });

    worker.on('message', messageFromWorker => {
      return resolve(messageFromWorker);
    })

    worker.on('error', reject);

    worker.on('exit', code => {
      if (code !== 0) {
        reject(new Error(`Worker ${name} stopped with exit code ${code}`));
      }
    })
  })
}
