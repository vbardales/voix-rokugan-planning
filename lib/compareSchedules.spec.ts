import compareSchedules, { generateTests } from './compareSchedules';
import {
  defaultGame,

  firstPlayerRandom,
  logger,

  kevPlayer,
  nelimPlayer,

  saturdayNightSlot,
  sundayEveningSlot,
  noRandomizeRandom,
} from './testFixtures';
import * as getBestSchedule from './getBestSchedule';
import * as getScheduleLib from './getSchedule';
import * as _ from 'lodash';
import { BestGameReport, WISH_LEVELS } from './models';

const result1: BestGameReport = {
  best: {
    results: [],
    leftGames: [],
    leftSlots: [],
    leftPlayers: [],
    leftPlayersBySlot: {},
  },
  bestScore: 0,
  scores: [],
};
const result2: BestGameReport = {
  best: {
    results: [],
    leftGames: [],
    leftSlots: [],
    leftPlayers: [],
    leftPlayersBySlot: {},
  },
  bestScore: 0,
  scores: [],
};

const services = {
  logger,
  scoreRandom: firstPlayerRandom,
  collectionRandom: noRandomizeRandom,
}

describe('compareSchedules', () => {
  let spyGetSchedule: jest.SpyInstance;
  let spyBestSchedule: jest.SpyInstance;

  beforeEach(() => {
    spyGetSchedule = jest.spyOn(getScheduleLib, 'getSchedule');
    spyGetSchedule.mockResolvedValue(result1).mockResolvedValue(result2);

    spyBestSchedule = jest.spyOn(getBestSchedule, 'default');
    spyBestSchedule.mockReturnValue(result1);
  });

  afterEach(() => jest.restoreAllMocks());

  it('should call lib X times', async () => {
    await compareSchedules({
      players: [nelimPlayer],
      slots: [saturdayNightSlot],
      games: [defaultGame],
    }, services, getScheduleLib.getSchedule, 5);
    expect(spyGetSchedule.mock.calls.length).toEqual(5);
  });

  describe('players', () => {
    it('should priorize people with more NEED wishes', () => {
      const nelimWithMoreNeedWishesPlayer = {
        ...nelimPlayer,
        wishes: [{ id: 1, level: WISH_LEVELS.NEED, xor: [] }],
      }
      const nelimWithLessNeedWishesPlayer = {
        ...nelimPlayer,
        wishes: [{ id: 1, level: WISH_LEVELS.WISH, xor: [] }, { id: 1, level: WISH_LEVELS.WISH, xor: [] }],
      };

      const request = {
        players: [nelimWithLessNeedWishesPlayer, nelimWithMoreNeedWishesPlayer],
        slots: [saturdayNightSlot],
        games: [defaultGame],
      };
      expect(generateTests(request, 1, noRandomizeRandom)).toEqual([
        {
          ...request,
          players: [nelimWithMoreNeedWishesPlayer, nelimWithLessNeedWishesPlayer],
        },
      ]);
    });

    it('should priorize people with more WISH wishes (for same NEED wishes)', () => {
      const nelimWithLessWishWishesPlayer = {
        ...nelimPlayer,
        wishes: [{ id: 1, level: WISH_LEVELS.WISH, xor: [] }],
      }
      const nelimWithMoreWishWishesPlayer = {
        ...nelimPlayer,
        wishes: [{ id: 1, level: WISH_LEVELS.WISH, xor: [] }, { id: 1, level: WISH_LEVELS.WISH, xor: [] }],
      };

      const request = {
        players: [nelimWithLessWishWishesPlayer, nelimWithMoreWishWishesPlayer],
        slots: [saturdayNightSlot],
        games: [defaultGame],
      };
      expect(generateTests(request, 1, noRandomizeRandom)).toEqual([
        {
          ...request,
          players: [nelimWithMoreWishWishesPlayer, nelimWithLessWishWishesPlayer],
        },
      ]);
    });

    it('should randomize same level people', () => {
      const nelimWithSameWishesPlayer = {
        ...nelimPlayer,
      };

      const request = {
        players: [nelimWithSameWishesPlayer, nelimPlayer],
        slots: [saturdayNightSlot],
        games: [defaultGame],
      };

      const lastRandom = {
        name: 'last',
        getCollection: <T>(collection: T[]) => _.reverse(collection),
      };

      expect(generateTests(request, 1, lastRandom)).toEqual([
        {
          ...request,
          players: [nelimPlayer, nelimWithSameWishesPlayer],
        },
      ]);
    });
  });

  describe('games', () => {
    it('should priorize games with NEED wish', () => {
      const gameInNeed = {
        ...defaultGame,
        id: 2,
        wishLevel: WISH_LEVELS.NEED,
      };
      const gameInWish = {
        ...defaultGame,
        id: 3,
        wishLevel: WISH_LEVELS.WISH,
      };

      const request = {
        players: [nelimPlayer],
        slots: [saturdayNightSlot],
        games: [gameInWish, gameInNeed],
      };
      expect(generateTests(request, 1, noRandomizeRandom)).toEqual([
        {
          ...request,
          games: [gameInNeed, gameInWish],
        },
      ]);
    });

    it('should priorize games with longer sessions (if same wishLevel)', () => {
      const gameWithLongerSessions = {
        ...defaultGame,
        id: 2,
        sessions: {
          minNumber: NaN,
          maxNumber: 2,
        },
      };

      const request = {
        players: [nelimPlayer],
        slots: [saturdayNightSlot],
        games: [defaultGame, gameWithLongerSessions],
      };
      expect(generateTests(request, 1, noRandomizeRandom)).toEqual([
        {
          ...request,
          games: [gameWithLongerSessions, defaultGame],
        },
      ]);
    });

    it('should priorize games with more NEED wishes (if same sessions length and same wishLevel)', () => {
      const gameWithLessNeedWishes = {
        ...defaultGame,
        id: 2,
        wishes: [{ id: 1, level: WISH_LEVELS.WISH, xor: [] }, { id: 1, level: WISH_LEVELS.WISH, xor: [] }],
      };
      const gameWithMoreNeedWishes = {
        ...defaultGame,
        id: 3,
        wishes: [{ id: 1, level: WISH_LEVELS.NEED, xor: [] }],
      };

      const request = {
        players: [nelimPlayer],
        slots: [saturdayNightSlot],
        games: [gameWithLessNeedWishes, gameWithMoreNeedWishes],
      };
      expect(generateTests(request, 1, noRandomizeRandom)).toEqual([
        {
          ...request,
          games: [gameWithMoreNeedWishes, gameWithLessNeedWishes],
        },
      ]);
    });

    it('should priorize games with more WISH wishes (if same sessions length and NEED number)', () => {
      const gameWithMoreWishWishes = {
        ...defaultGame,
        id: 2,
        wishes: [{ id: 1, level: WISH_LEVELS.NEED, xor: [] }, { id: 1, level: WISH_LEVELS.WISH, xor: [] }],
      };
      const gameWithLesWishWishes = {
        ...defaultGame,
        id: 3,
        wishes: [{ id: 1, level: WISH_LEVELS.NEED, xor: [] }],
      };

      const request = {
        players: [nelimPlayer],
        slots: [saturdayNightSlot],
        games: [gameWithLesWishWishes, gameWithMoreWishWishes],
      };
      expect(generateTests(request, 1, noRandomizeRandom)).toEqual([
        {
          ...request,
          games: [gameWithMoreWishWishes, gameWithLesWishWishes],
        },
      ]);
    });

    it('should priorize games with more required minimum players', () => {
      const gameWithLessWishes = {
        ...defaultGame,
        id: 2,
        players: {
          minNumber: 0,
          maxNumber: NaN,
        }
      };

      const request = {
        players: [nelimPlayer],
        slots: [saturdayNightSlot],
        games: [gameWithLessWishes, defaultGame],
      };
      expect(generateTests(request, 1, noRandomizeRandom)).toEqual([
        {
          ...request,
          games: [defaultGame, gameWithLessWishes],
        },
      ]);
    });

    it('should randomize same level games', () => {
      const gameWithSameWishes = {
        ...defaultGame,
        id: 2,
      };

      const request = {
        players: [nelimPlayer],
        slots: [saturdayNightSlot],
        games: [gameWithSameWishes, defaultGame],
      };

      const lastRandom = {
        name: 'last',
        getCollection: <T>(collection: T[]) => _.reverse(collection),
      };
      expect(generateTests(request, 1, lastRandom)).toEqual([
        {
          ...request,
          games: [defaultGame, gameWithSameWishes],
        },
      ]);
    });
  });

  it('should randomize slots', () => {
    const lastRandom = {
      name: 'last',
      getCollection: <T>(collection: T[]) => _.reverse(collection),
    };

    const request = {
      players: [nelimPlayer],
      slots: [saturdayNightSlot, sundayEveningSlot],
      games: [defaultGame],
    };
    expect(generateTests(request, 1, lastRandom)).toEqual([
      {
        ...request,
        slots: [sundayEveningSlot, saturdayNightSlot],
      },
    ]);
  });

  it('should return all schedules', async () => {
    expect(await compareSchedules({
      players: [nelimPlayer, kevPlayer],
      slots: [saturdayNightSlot],
      games: [defaultGame],
    }, services, getScheduleLib.getSchedule, 2)).toEqual({
      schedules: [
        result1,
        result2,
      ],
      bestReport: result1,
    });
  });

  it('should elect the best schedule if any', async () => {
    expect(await compareSchedules({
      players: [nelimPlayer, kevPlayer],
      slots: [saturdayNightSlot],
      games: [defaultGame],
    }, services, getScheduleLib.getSchedule, 2)).toEqual({
      schedules: [
        result1,
        result2,
      ],
      bestReport: result1,
    });

    expect(getBestSchedule.default).toHaveBeenCalledWith([
      result1,
      result2,
    ], [nelimPlayer, kevPlayer]);
  });

  it('should elect none otherwise', async () => {
    spyBestSchedule.mockReturnValue(null);

    expect(await compareSchedules({
      players: [nelimPlayer, kevPlayer],
      slots: [saturdayNightSlot],
      games: [defaultGame],
    }, services, getScheduleLib.getSchedule, 2)).toEqual({
      schedules: [
        result1,
        result2,
      ],
      bestReport: null,
    });
  });
});
