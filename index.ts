import { randoms, shufflingRandom } from './lib/testFixtures';
import compareSchedules from './lib/compareSchedules';
import { getSchedule } from './lib/getSchedule';
import logResult, { logPlayerResult } from './lib/logResult';
import { importSpreadsheet, importSlotsFromJson } from './lib/imports';
import getFileLogger from './lib/fileLogger';

const ITERATIONS = 100;

async function main() {
  console.info('WELCOME');
  const date = Date.now();

  const debugLogger = getFileLogger('debug.txt');
  debugLogger.info('Loading spreadsheet');
  const { players, games } = await importSpreadsheet(debugLogger);

  debugLogger.info('Loading slots');
  const slots = importSlotsFromJson(debugLogger);

  debugLogger.info('Starting schedules');
  const request = {
    players,
    slots,
    games,
  };
  const result = await compareSchedules(request, {
    logger: debugLogger,
    scoreRandom: randoms.byPlayerState,
    collectionRandom: shufflingRandom,
  }, getSchedule, ITERATIONS );

  console.info(`HERE IS THE RESULT for ${ITERATIONS} iterations: ${result.bestReport ? 'we have a winner!' : 'no result found'}`);
  logResult(request, result.bestReport, console);
  logResult(request, result.bestReport, getFileLogger('report.txt'));

  players.forEach((player) => logPlayerResult(player, request, result.bestReport, getFileLogger(`players/${player.pseudo}`)));

  console.debug(`Found result in ${(Date.now() - date) / 1000}s`);

  console.info('GOODBYE');
}

main();
