Planning solver
==============

Problem to solve: trying to solve game initialization and scheduling since it's an essential, long, meaningless task.

For a given set of players, time slots and games, choose the more efficient schedule.

# Setup

Clone `.env.dist` file as `.env` file and setup properties.
Use `players.json`, `games.json`, `slots.json` to provide data to the algorithm, based on the schema described in `lib/models.ts`.

Start the algorithm using CLI command `npm run start`. The result will be displayed but not persisted yet.

# Improvals

- handle avoid slots on players and games
- more priority if not already played with those GMs/players
- clans + cooking handling
- a by-game report
- integrate boardgames
- better display (?)

# Installation

Requiring nodejs v14 at least, use `npm install` to bootstrap for dev, you should use `npm ci` for production purpose.

# Devs

Built over TDD, use `npm run test:watch` to launch all tests and iterate.
